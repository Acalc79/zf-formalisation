{-# OPTIONS --exact-split #-}
module ZF.WellFoundedness where

open import ZF.Foundation
open import ZF.Function

open import ZF.Foundation.WellFoundedness public

private variable S G R X : set

module _ (wf : well-founded R X) where
  wf-rec :
    {Y : set} →
    G ∶ 𝒫 (X × Y) ⟶ Y
    → ----------------------------------------
    set
  wf-rec {G = G} wf Y p = {!!}

  init : set → set
  init x = ｛ y ∈ X ∣ y < x ｝

  module _ (G-fun : G ∶ 𝒫 (X b× Y) ⟶ Y) where
    private f = wf-rec G
    
    wf-rec-fun : f ∶ X ⟶ Y
    wf-rec-fun = ?

    wf-rec-prop : f [ x ] ≡ G [ restrict f (I x) ] 
    wf-rec-prop = ?
