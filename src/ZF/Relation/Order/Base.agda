{-# OPTIONS --exact-split #-}
module ZF.Relation.Order.Base where

open import ZF.Foundation

module OrderVars where
  variable X Y Z W X' Y' R RX RY RZ RX' RY' S T x x' y y' : set
open OrderVars

module Notation (R : set) where

  _<_ _≮_ _≤_ _≰_ : (x y : set) → Set
  
  x < y = ⟨ x ، y ⟩ ∈ R
  x ≮ y = ¬ x < y
  x ≤ y = x < y ∨ x ≡ y
  x ≰ y = ¬ x ≤ y

module Relation X (rel : R ∶ X ⇸ X) where
  open Notation R public

  x<y→x∈X : x < y → x ∈ X
  x<y→x∈X x<y = proj₁ $ from ⟨,⟩∈× $ rel x<y

  x<y→y∈X : x < y → y ∈ X
  x<y→y∈X x<y = proj₂ $ from ⟨,⟩∈× $ rel x<y
