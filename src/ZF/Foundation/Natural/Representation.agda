{-# OPTIONS --exact-split #-}
module ZF.Foundation.Natural.Representation where

open import ZF.Foundation.Axiom
open import ZF.Foundation.Natural.Induction
open import ZF.Foundation.Natural.Literal
open import ZF.Foundation.WellFoundedness
open import ZF.Foundation.Properties

is-nat : (x : set) → Set
is-nat x =
  (x ≡ ∅ ∨ ∃[ y ] (x ≡ y ⁺)) ∧
  (⋀ n ∈ x , n ≡ ∅ ∨ (⋁ y ∈ x , n ≡ y ⁺))

open import Data.Sum

private variable x y z : set

abstract
  nat1 : is-nat 1
  nat1 = inj₂ (∅ , refl) , inj₁ ∘ fromInj₂ (⊥-elim ∘ ∈∅) ∘ from ∈⁺

  nat⁺ : is-nat x → is-nat (x ⁺)
  nat⁺ (inj₁ refl , _) = nat1
  nat⁺ (inj₂ (l , refl) , p) =
    inj₂ (l ⁺ , refl) ,
    λ x∈l⁺⁺ → case from ∈⁺ x∈l⁺⁺ of λ
    { (inj₁ x∈l⁺) →
      map₂ (λ { (y , y∈l⁺ , refl) → y , to ∈⁺ (inj₁ y∈l⁺) , refl}) $ p x∈l⁺
    ; (inj₂ refl) → inj₂ (l , to ∈⁺ (inj₁ $ to ∈⁺ $ inj₂ refl) , refl)}

  nat-rec : is-nat x → x ≡ 0 ∨ (⋁ y ∈ x , (x ≡ y ⁺ ∧ is-nat y))
  nat-rec natx@(px , py∈x) = flip map₂ px λ
    { (y , refl) → let y∈x = to ∈⁺ (inj₂ refl) in
      y , y∈x , refl ,
      map₂ (λ { (z , _ , y≡z⁺) → z , y≡z⁺}) (py∈x y∈x) ,
      (λ {x} x∈y →
        map₂ (λ { (z , z∈y⁺ , refl) → case from ∈⁺ z∈y⁺ of λ
          { (inj₁ z∈y) → z , z∈y , refl
          ; (inj₂ refl) → ⊥-elim $ asym-∈ z∈y⁺ x∈y}}) $
        py∈x $ to ∈⁺ $ inj₁ x∈y)}

  nat∈ω : ∀{x} → x ∈ ω ⇔ is-nat x
  nat∈ω = mk⇔
    (is-nat x by-induction-on x
      [base: inj₁ refl , ⊥-elim ∘ ∈∅
      ,step: (λ
      { _ (inj₁ refl , _) → nat1        
      ; y⁺∈ω (inj₂ (y , refl) , x∈y⁺→x≡0∨x≡z⁺) →
        inj₂ (y ⁺ , refl) ,
        λ x∈y⁺⁺ → case from ∈⁺ x∈y⁺⁺ of λ
        { (inj₁ x∈y⁺) →
          map₂ (λ { (z , z∈y⁺ , refl) → z , to ∈⁺ (inj₁ z∈y⁺) , refl}) $
          x∈y⁺→x≡0∨x≡z⁺ x∈y⁺
        ; (inj₂ refl) → inj₂ (y , to ∈⁺ (inj₁ $ to ∈⁺ $ inj₂ refl) , refl) }})
      ])
    (nat→∈ω _)
    where nat→∈ω : ∀ x → is-nat x → x ∈ ω
          nat→∈ω = (is-nat x → x ∈ ω) by-∈-induction-on x
            step: λ {x IH natx → case nat-rec natx of λ
              { (inj₁ refl) → ∅∈ω
              ; (inj₂ (y , y∈x , refl , naty)) →
                to ∈ω $ inj₂ $ y , IH y∈x naty , refl }}
