{-# OPTIONS --exact-split #-}
module ZF.Relation.Order.Well.PartialOrder where

open import ZF.Foundation as F hiding (_∘_)
open import ZF.Relation.Order.Total

open import Data.Product using (Σ-syntax)

order-≤ : ∀ X RX Y RY → Set
order-≤ X RX Y RY =
  ∃ λ Z → Z is-initial-segment-of[ Y , RY ] ∧ isomorphic RX X RY Z

order-< : ∀ X RX Y RY → Set
order-< X RX Y RY =
  ∃ λ Z → Z ⊂ Y ∧ Z is-initial-segment-of[ Y , RY ] ∧ isomorphic RX X RY Z

open import ZF.Relation.Order.Base
open import ZF.Relation.Order.Well.Base
open OrderVars

_≤_ : .(is-well-ordering RX X) → .(is-well-ordering RY Y) → Set
_≤_ {RX}{X}{RY}{Y} _ _ = order-≤ X RX Y RY

_<_ : .(is-well-ordering RX X) → .(is-well-ordering RY Y) → Set
_<_ {RX}{X}{RY}{Y} _ _ = order-< X RX Y RY

≤-refl : {wo-X : is-well-ordering RX X} → wo-X ≤ wo-X
≤-refl {RX}{X}{wf , R-tot@(R-rel , _)} =
  X , self-initial R-rel , isomorphic-refl R-tot

open import ZF.Function as F hiding (restrict)
open import ZF.Relation.Base
open import ZF.Reasoning

private module B = WithBijection

≤-trans : {wo-X : is-well-ordering RX X}
          {wo-Y : is-well-ordering RY Y}
          {wo-Z : is-well-ordering RZ Z}
          → ----------------------------------------
          wo-X ≤ wo-Y → wo-Y ≤ wo-Z → wo-X ≤ wo-Z
≤-trans {RX}{X}{RY}{Y}{RZ}{Z}
  (y , (y⊆Y , init-y) , f , (f-fun@(refl , _) , f-bij) , mon)
  (z , (z⊆Z , init-z) , g , (g-fun@(refl , _) , g-bij) , mon')
  with refl ← B.ran≡ f-fun f-bij | refl ← B.ran≡ g-fun g-bij =
  z' , z'-init , h , (h-fun , {!!}) , {!!}
  where
  g' = F.restrict g (ran f)
  h = g' ∘ f
  z' = ran g'
  g'-fun : g' ∶ ran f ⟶ z'
  g'-fun = ⟶-tighten $ restrict-fun y⊆Y g-fun
  h-fun : h ∶ X ⟶ z'
  h-fun = ∘-fun g'-fun f-fun
  z'⊆Z : z' ⊆ Z
  z'⊆Z = begin⊆ z' ⊆⟨ ran-restrict⊆ ⟩
                z ∩ z ≡⟨ ∩-idemp ⟩
                z ⊆⟨ z⊆Z ⟩
                Z ∎
  z'-init : z' is-initial-segment-of[ Z , RZ ]
  z'-init = z'⊆Z , λ {z₀} z₀∈z' {z₁} z₁<z₀ → case from ∈ran z₀∈z' of λ
    { (y₀ , y₀z₀∈rg) → case from ⟨,⟩∈restrict y₀z₀∈rg of λ
    { (y₀z₀∈g , y₀∈ranf , z₀∈rang) → case from ∈ran y₀∈ranf of λ
    { (x₀ , x₀y₀∈f) →
      let z₁∈rang = init-z z₀∈rang z₁<z₀ in
      case from ∈ran z₁∈rang  of λ
    { (y₁ , y₁z₁∈g) →
      to ∈ran $ y₁ , to ⟨,⟩∈restrict (
      y₁z₁∈g ,
      {!init-y y₀∈ranf!} ,
      z₁∈rang)
      }}}}
∑_(n=0)^x (#(k,l) s.t. k+l = m-x) = ∑_(n=0)^x (m-x+1) = 1/2*(x+1)(x+2)
