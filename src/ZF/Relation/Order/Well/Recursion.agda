{-# OPTIONS --exact-split #-}
open import ZF.Foundation hiding (_[_∈dom])
open import ZF.Function as F hiding (restrict; _∘_)
open import ZF.Relation.Order.Well.Base

module ZF.Relation.Order.Well.Recursion
  {X}{R}(well-ordered : is-well-ordering R X)
  {Y}{G}(G-fun : G ∶ 𝒫 (X × Y) ⟶ Y)
  where

open import ZF.Reasoning
open import ZF.Relation.Order.Well.Recursion.Internals well-ordered G-fun

open WellOrdered well-ordered
open FunReify θ !θ renaming (reify to f)

wf-rec' : set
wf-rec' = f

abstract
  wf-rec : set
  wf-rec = f

  wf-rec-expand : wf-rec ≡ wf-rec'
  wf-rec-expand = refl

  wf-rec-fun : wf-rec ∶ X ⟶ Y
  wf-rec-fun =
    ∶⟶-cong-⊆ reify-dom (λ {y} y∈ranf → case from ∈ran y∈ranf of λ
      { (x , xy∈f) → let x∈X = subst (x ∈_) reify-dom $ to ∈dom $ y , xy∈f in
        case reify-valid x∈X of λ
      { (h , (_ , h-fun@(_ , _ , h-rel) , _) , x∈h , h[x]≡f[x]) →
      let xf[x]∈h = from (h-fun [ x ]≡) $ x∈h , h[x]≡f[x] in
      subst (_∈ Y) (begin h [ x ] ≡⟨ h[x]≡f[x] ⟩
                          f [ x ] ≡⟨ proj₂ $ to (f-fun' [ x ]≡) xy∈f ⟩
                          y ∎) $
      val∈ran h-fun x∈h} }) f-fun'
    where f-fun' = is-set-fun→∶⟶ reify-is-fun
    
  private
    attempt⊆f : ∀{h} → attempt h → h ⊆ f
    
  attempt⊆f {h}
    att-h@((domh⊆X , h-init) , h-fun@(_ , _ , h-rel) , h[x]≡G[h])
    {e} e∈h =
    case from ∈× $ h-rel e∈h of λ{ (x , x∈h , y , y∈Y , refl) →
    let x∈X = domh⊆X x∈h in
    case reify-valid x∈X of λ
    {(h' , att-h'@(h'-init , h'-fun , h'[x]≡G[h']) , x∈h' , h'[x]≡f[x]) →
    from (wf-rec-fun [ x ]≡) $
    x∈X , (begin
      f [ x ] ≡⟨ sym h'[x]≡f[x] ⟩
      h' [ x ] ≡⟨ !attempt (att-h' , x∈h') (att-h , x∈h) ⟩
      h [ x ] ≡⟨ (proj₂ $ to (h-fun [ x ]≡) e∈h) ⟩
      y ∎)}}
    
  wf-rec-prop : ⋀ x ∈ X , wf-rec [ x ] ≡ G [ F.restrict wf-rec (init x) ] 
  wf-rec-prop {x} x∈X = case reify-valid x∈X of λ
    { (h , att-h@(h-init , h-fun , h[x]≡G[h]) , x∈h , h[x]≡f[x]) →
      begin f [ x ] ≡⟨ sym h[x]≡f[x] ⟩
            h [ x ] ≡⟨ h[x]≡G[h] x∈h ⟩
            G [ F.restrict h (init x) ]
              ≡⟨ cong (G [_]) $
                 restrict≡restrict (init⊆initial h-init x∈h)
                   (attempt⊆f att-h) wf-rec-fun ⟩
            G [ F.restrict f (init x) ] ∎}

  ran-wf-rec : ran wf-rec ⊆ ran G
  ran-wf-rec {y} y∈f = case from ∈ran y∈f of λ{ (x , xy∈f) →
    case to (f-fun [ x ]≡) xy∈f of λ{ (x∈X , refl) →
    subst (_∈ ran G) (sym $ wf-rec-prop x∈X) $
    to ∈ran $
    F.restrict f (init x) ,
    let f'∈Rel : F.restrict f (init x) ∈ Rel X Y
        f'∈Rel = to ∈𝒫 $ trans-⊆ restrict⊆ $ proj₂ $ proj₂ $ f-fun
    in G-fun [ f'∈Rel ∈dom]}}
    where f-fun = wf-rec-fun
