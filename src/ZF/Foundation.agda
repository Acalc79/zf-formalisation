{-# OPTIONS --exact-split #-}
module ZF.Foundation where

open import ZF.Foundation.Axiom public
open import ZF.Foundation.Pair public
open import ZF.Foundation.Relation public
open import ZF.Foundation.Function public
  renaming (module Reify to FunReify; module Dereify to FunDereify)
open import ZF.Foundation.Natural public
open import ZF.Foundation.RecursiveDefinition public
open import ZF.Foundation.WellFoundedness public
open import ZF.Foundation.Properties public
