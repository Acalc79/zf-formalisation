{-# OPTIONS --exact-split #-}
module ZF.Reasoning where

open import ZF.Foundation.Axiom
open import Relation.Binary
open import Relation.Binary.PropositionalEquality

⊆-poset : Poset _ _ _
⊆-poset = record
  { Carrier = set
  ; _≈_ = _≡_
  ; _≤_ = _⊆_
  ; isPartialOrder = record
    { isPreorder = record
      { isEquivalence = isEquivalence
      ; reflexive = reflexive-⊆
      ; trans = trans-⊆ }
    ; antisym = antisym-⊆ } }

open import Relation.Binary.Reasoning.PartialOrder ⊆-poset as Base public
  renaming (begin_ to begin⊆_; begin-equality_ to begin_; begin-strict_ to begin⊂_)
  hiding (step-≈; step-≈˘; step-<; step-≤)

step-⊆ : ∀ x {y z} → y IsRelatedTo z → x ⊆ y → x IsRelatedTo z
step-⊆ = Base.step-≤

infixr 2 step-⊆
syntax step-⊆ x y≡z x⊆y = x ⊆⟨  x⊆y ⟩ y≡z
