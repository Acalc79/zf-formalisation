{-# OPTIONS --safe --exact-split --without-K #-}
module Logic where

open import Level
open import Function public using (mk⇔) renaming (_⇔_ to infix -1 _⇔_)
open Function.Equivalence renaming (f to from; g to to) using () public
open import Relation.Nullary using (¬_; yes; no) public
open import Relation.Binary.PropositionalEquality
  using (_≡_; refl; _≢_; sym;
         subst; subst₂; cong; cong₂; trans; module ≡-Reasoning) public
open import Data.Empty using (⊥; ⊥-elim) public
open import Data.Sum renaming (_⊎_ to _∨_) using (inj₁; inj₂) public
open import Data.Product as Prod
  renaming (Σ to infixr 2 _∧ᵈ_; _×_ to _∧_; proj₁ to elem; proj₂ to prop) public
  using (_,_; ∃-syntax; ∃; ∃₂)
open Prod using (proj₁; proj₂) public

module LogicVars where
  variable l a b : Level
           A B C A' B' C' : Set l
           P P' Q Q' R R' : A → Set l

open LogicVars

∃! : {A : Set a}(P : A → Set b) → Set (a ⊔ b)
∃! = Prod.∃! _≡_

∃∧!→∃! : ∃ P → (∀{x y} → P x → P y → x ≡ y) → ∃! P
∃∧!→∃! (x , Px) !P = x , Px , !P Px

