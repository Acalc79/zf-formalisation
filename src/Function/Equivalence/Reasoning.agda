{-# OPTIONS --safe --without-K --exact-split #-}
module Function.Equivalence.Reasoning where

open import Level
open import Function using (_⇔_) public
open import Function.Properties.Equivalence
open import Relation.Binary hiding (_⇔_)

⇔-setoid : ∀{c} → Setoid (suc c) c
⇔-setoid {c} = record
  { Carrier = Set c
  ; _≈_ = _⇔_
  ; isEquivalence = ⇔-isEquivalence }

module _ {c} where
  open import Relation.Binary.Reasoning.Setoid (⇔-setoid {c}) as Base public
    hiding (step-≈; step-≈˘)
  infixr 2 step-⇔ step-⇔˘
  
  step-⇔ = Base.step-≈
  step-⇔˘ = Base.step-≈˘
  
  syntax step-⇔ x y⇔z x⇔y = x ⇔⟨ x⇔y ⟩ y⇔z
  syntax step-⇔˘ x y⇔z y⇔x = x ⇔˘⟨ y⇔x ⟩ y⇔z
