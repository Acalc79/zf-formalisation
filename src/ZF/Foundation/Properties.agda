{-# OPTIONS --exact-split #-}
module ZF.Foundation.Properties where

open import ZF.Foundation.Axiom
open import ZF.Foundation.Axiom.Nonconstructive
open import ZF.Foundation.Pair

private variable x y z : set

abstract
  asym-∈ : x ∈ y → y ∉ x
  asym-∈ {x}{y} x∈y y∈x = case regularity ｛ x ، y ｝ nonempty-xy of λ
    { (z , z∈xy , ¬∃k,k∈xy∧k∈z) → case from ∈xy z∈xy of λ
    { (inj₁ refl) → ¬∃k,k∈xy∧k∈z $ y , to ∈xy (inj₂ refl) , y∈x
    ; (inj₂ refl) → ¬∃k,k∈xy∧k∈z $ x , to ∈xy (inj₁ refl) , x∈y } }
    where ∈xy = ∈｛ x ، y ｝
          nonempty-xy : nonempty ｛ x ، y ｝
          nonempty-xy = inhabited→nonempty $ x , to ∈xy (inj₁ refl)

