{-# OPTIONS --exact-split #-}
open import ZF.Foundation hiding (_[_∈dom])
open import ZF.Function as F hiding (_∘_; restrict)
open import ZF.Relation.Order.Well.Base

module ZF.Relation.Order.Well.Recursion.Internals {X}{R}{Y}{G}
  (well-ordered : is-well-ordering R X)
  (G-fun : G ∶ 𝒫 (X × Y) ⟶ Y)
  where

open import ZF.Relation.Order.Total as Total hiding (module Order)

open import ZF.Relation.Order.Base
open import ZF.Relation.Base hiding (_∘_)
open import ZF.Reasoning

private variable I S RX RY x y z : set

open Initial X R
open WellOrdered well-ordered

abstract
  domG : dom G ≡ 𝒫 (X × Y)
  domG = proj₁ G-fun
  !G : ∀{x y y'} → ⟨ x ، y ⟩ ∈ G → ⟨ x ، y' ⟩ ∈ G → y ≡ y'
  !G = proj₁ (proj₂ G-fun)
  G-rel : G ∶ 𝒫 (X × Y) ⇸ Y
  G-rel = proj₂ (proj₂ G-fun)
    
attempt : set → Set
attempt h = let I = dom h in
  I is-initial ∧
  h ∶ I ⟶ Y ∧
  (⋀ x ∈ I , h [ x ] ≡ G [ F.restrict h (init x) ])

abstract
  ∃attempt : ⋀ x ∈ X , ∃ λ h → attempt h ∧ x ∈ dom h
  !attempt : ∀{h h' x} →
    attempt h ∧ x ∈ dom h →
    attempt h' ∧ x ∈ dom h'
    → ------------------------------
    h [ x ] ≡ h' [ x ]

  x∈attempt→x∈X : ∀{h} → attempt h → x ∈ dom h → x ∈ X

  !attempt {h}{h'}{x}
    (attempt-h@(init-domh@(domh⊆X , _) , h-fun , h[x]≡G[h|Ix]) , x∈h)
    ((init-domh' , h'-fun , h'[x]≡G[h'|Ix]) , x∈h') =
       to ∈∩ (x∈h , x∈h') ∶ x ∈ I'
    |> ((h [ x ] ≡ h' [ x ]) by-wf[ I-wf ]-induction-on x step:
      λ {x'} x'∈I IH → begin
        h [ x' ]
          ≡⟨ h[x]≡G[h|Ix] $ A∩B⊆A x'∈I ⟩
        G [ h| x' ]
          ≡⟨ (let Ix'⊆h = init⊆initial init-domh $ A∩B⊆A x'∈I
                  Ix'⊆h' = init⊆initial init-domh' $ A∩B⊆B x'∈I
                  Ix'⊆I = init⊆initial (∩-initial init-domh init-domh') x'∈I in
             cong (G [_]) $ fun-ext
             (restrict-fun Ix'⊆h h-fun)
             (restrict-fun Ix'⊆h' h'-fun)
             (λ {y} y∈Ix' → begin
               h| x' [ y ] ≡⟨ restrict-to h-fun , Ix'⊆h  [ y∈Ix' ]≡ ⟩
               h [ y ] ≡⟨ IH (Ix'⊆I y∈Ix') $
                          to ⟨,⟩∈restrict $
                          from ∈init y∈Ix' , Ix'⊆I y∈Ix' , x'∈I ⟩ -- 
               h' [ y ] ≡⟨ sym $ restrict-to h'-fun , Ix'⊆h' [ y∈Ix' ]≡ ⟩
               h'| x' [ y ] ∎) )⟩
        G [ h'| x' ]
          ≡⟨ sym $ h'[x]≡G[h'|Ix] $ A∩B⊆B x'∈I ⟩
        h' [ x' ] ∎)
    where I' = dom h ∩ dom h'
          I-wf : well-founded (restrict R I' I') I'
          I-wf = proj₁ $ well-ordered-⊆ $ trans-⊆ A∩B⊆A domh⊆X
          h| = λ x → F.restrict h (init x)
          h'| = λ x → F.restrict h' (init x)

  x∈attempt→x∈X ((dom-h⊆X , _) , _ , _) x∈h = dom-h⊆X x∈h

  P : (x : set) → Set
  P = λ x → ∃ λ h → attempt h ∧ dom h ≡ init x ∪ ｛ x ｝

  ∃attempt' : ⋀ x ∈ X , P x

  open import Data.Product using (map₂)

  ∃attempt {x} x∈X =
    map₂ (map₂ λ p → subst (x ∈_) (sym p) $ to ∈∪ $ inj₂ $ to ∈｛ x ｝ refl) $
    ∃attempt' x∈X

  inductive-step : ⋀ x ∈ X , ((⋀ y ∈ X , (y < x → P y)) → P x)

  ∃attempt' = (∃ λ h → attempt h ∧ dom h ≡ init x ∪ ｛ x ｝)
    by-wf[ wf ]-induction-on x step: inductive-step

  inductive-step {x} x∈X IH =
    h' ,
    (subst _is-initial (sym domh'≡) (init-step x∈X) ,
     h'-fun ,
     λ {z} z∈domh' → case from ∈∪ $ subst (z ∈_) domh'≡ z∈domh' of λ
       { (inj₁ z∈initx) → let z<x = from ∈init z∈initx in
         case IH (x<y→x∈X z<x) z<x of λ
         { (h₀ ,
            att@(init-h₀ , h₀-fun@(refl , _ , h₀-rel) , h₀[_]) ,
            domh₀≡) →
         let z∈domh₀ = subst (z ∈_) (sym domh₀≡) $
                       to ∈∪ $ inj₂ $ to ∈｛ z ｝ refl
             h₀⊆h' : h₀ ⊆ h'
             h₀⊆h' {v} v∈h₀ = case from ∈× $ h₀-rel v∈h₀ of λ
               { (x' , x'∈h₀ , y' , y'∈Y , refl) →
                 to ∈∪ $ inj₁ $ to ∈⋃ $
                 h₀ ,
                 to ∈sep (
                   Rel-cong-⊆ (subst (_⊆ init x) (sym domh₀≡) $
                               A⊆X∧B⊆X→A∪B⊆X (<-init⊆ z<x) $
                               to unit-⊆ $ to ∈init z<x)
                     refl-⊆ (to ∈Rel h₀-rel) , att) ,
                 v∈h₀}
         in begin
         h' [ z ] ≡⟨ sym $ ⊆[]≡ h₀⊆h' h'-fun z∈domh₀ ⟩
         h₀ [ z ] ≡⟨ h₀[ z∈domh₀ ] ⟩
         G [ F.restrict h₀ (init z) ]
           ≡⟨ cong (G [_]) $ restrict≡restrict
                (subst (init z ⊆_) (sym domh₀≡) A⊆A∪B)
                h₀⊆h' h'-fun ⟩
         G [ F.restrict h' (init z) ] ∎}
       ; (inj₂ z∈x) → case from ∈｛ x ｝ z∈x of λ {refl → begin
         h' [ x ] ≡⟨ proj₂ $ to (h'-fun [ x ]≡) $
                     to ∈∪ $ inj₂ $ to ∈｛ _ ｝ refl ⟩
         G [ h ] ≡⟨ cong (G [_]) $ sym restrict-h' ⟩
         G [ F.restrict h' (init x)] ∎}}) ,
    domh'≡
    where
      h = ⋃ ｛ hy ∈ Rel (init x) Y ∣ attempt hy ｝
      h' = h ∪ ｛ ⟨ x ، G [ h ] ⟩ ｝
      ∈sep = ∈｛ hy ∈ Rel (init x) Y ∣ attempt hy ｝
      domh≡ : dom h ≡ init x
      domh≡ = begin
        dom h ≡⟨⟩
        dom (⋃ ｛ hy ∈ Rel (init x) Y ∣ attempt hy ｝)
          ≡⟨ dom⋃ ⟩
        ⋃ ｛ dom y ∣ y ∈ ｛ hy ∈ Rel (init x) Y ∣ attempt hy ｝ ｝
          ≡⟨ antisym-⊆
              (λ {y} y∈S → case from ∈⋃ y∈S of λ
                { (d , d∈rep , y∈d) → case from (∈fun-rep dom) d∈rep of λ
                { (hy , hy∈sep , refl) →
                  case from ∈sep hy∈sep of λ
                { (hy∈Rel , (hy⊆X , _) , _) → dom⊆ (from ∈Rel hy∈Rel) y∈d }}})
              (λ {y} y∈init → let y<x = from ∈init y∈init in
                  case IH (x<y→x∈X y<x) y<x of λ
                { (h' , att-h'@(_ , (_ , _ , rel-h') , _) , domh'≡init∪y) →
                  to ∈⋃ $
                  dom h' ,
                  to (∈fun-rep dom) (
                    h' ,
                    to ∈sep (
                      Rel-cong-⊆ (begin⊆
                        dom h' ≡⟨ domh'≡init∪y ⟩
                        init y ∪ ｛ y ｝
                          ⊆⟨ A⊆X∧B⊆X→A∪B⊆X (<-init⊆ y<x)
                               (to unit-⊆ $ to ∈init y<x) ⟩
                        init x ∎) refl-⊆ (to ∈Rel rel-h') ,
                      att-h') ,
                    refl) ,
                  subst (y ∈_) (sym domh'≡init∪y)
                    (to ∈∪ $ inj₂ $ to ∈｛ y ｝ refl)}) ⟩
        init x ∎
      domh'≡ = begin dom h' ≡⟨ dom-step h x (G [ h ]) ⟩
                     dom h ∪ ｛ x ｝ ≡⟨ cong (_∪ ｛ x ｝) domh≡ ⟩
                     init x ∪ ｛ x ｝ ∎
      h-rel : h ∶ init x ⇸ Y
      h-rel = ⋃-rel λ {y} y∈sep →
        proj₁ (from ∈sep y∈sep) ∶ y ∈ Rel (init x) Y
      h-fun : h ∶ init x ⟶ Y
      h-fun = domh≡ ,
        (λ {x₀}{y₀}{y₁} xy₀∈h xy₁∈h →
          case from ∈⋃ xy₀∈h , from ∈⋃ xy₁∈h of λ
          { ((h₀ , h₀∈sep , xy₀∈h₀) , h₁ , h₁∈sep , xy₁∈h₁) →
          case from ∈sep h₀∈sep , from ∈sep h₁∈sep of λ
          { ((rel-h₀ , att-h₀@(_ , fun-h₀ , _)) ,
             (rel-h₁ , att-h₁@(_ , fun-h₁ , _))) →
          begin y₀ ≡⟨ sym $ proj₂ $ to (fun-h₀ [ x₀ ]≡) xy₀∈h₀ ⟩
                h₀ [ x₀ ] ≡⟨ !attempt (att-h₀ , to ∈dom (y₀ , xy₀∈h₀))
                                      (att-h₁ , to ∈dom (y₁ , xy₁∈h₁)) ⟩
                h₁ [ x₀ ] ≡⟨ proj₂ $ to (fun-h₁ [ x₀ ]≡) xy₁∈h₁ ⟩
                y₁ ∎}}) ,
        h-rel
      h'-fun : h' ∶ dom h' ⟶ Y
      h'-fun = subst₂ (h' ∶_⟶_) (sym domh'≡)
                      (Rel-cong-⊆ init⊆ refl-⊆ (to ∈Rel h-rel) ∶ h ∈ Rel X Y
                      |> val∈ran G-fun ∶ G [ h ] ∈ Y
                      |> ∪-cancelʳ ∘ to unit-⊆  ) $
        ∪-fun (λ z (z∈init-x , z∈x) → case from ∈｛ x ｝ z∈x of λ
                 {refl → <-irrefl $ from ∈init z∈init-x})
        h-fun unit-fun
      initx⊆domh' : init x ⊆ dom h'
      initx⊆domh' = begin⊆
        init x ⊆⟨ A⊆A∪B ⟩
        init x ∪ ｛ x ｝ ≡⟨ sym domh'≡ ⟩
        dom h' ∎
      restrict-h' : F.restrict h' (init x) ≡ h
      restrict-h' =
        fun-ext (restrict-fun initx⊆domh' h'-fun)
          h-fun
          λ {z} z∈initx → begin
            F.restrict h' (init x) [ z ]
              ≡⟨ restrict-to h'-fun , initx⊆domh' [ z∈initx ]≡ ⟩
            h' [ z ]
              ≡⟨ proj₂ $ to (h'-fun [ z ]≡) $
                 A⊆A∪B $ h-fun [ z∈initx ∈dom] ⟩
            h [ z ] ∎

θ = λ x y → ∃ λ h → attempt h ∧ x ∈ dom h ∧ h [ x ] ≡ y

abstract
  !θ : ⋀ x ∈ X , ∃! (θ x)
  !θ {x} x∈X = case ∃attempt x∈X of λ {(h , attempt-h , x∈h) →
    h [ x ] , (h , attempt-h , x∈h , refl) ,
    λ { {y}(h' , attempt-h' , x∈h' , refl) →
    !attempt (attempt-h , x∈h) (attempt-h' , x∈h')}}
