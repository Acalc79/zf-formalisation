{-# OPTIONS --exact-split #-}
module ZF.Foundation.Pair.Properties where

open import ZF.Foundation.Axiom
open import ZF.Foundation.Pair.Base
open import ZF.Foundation.Pair.Construction

private
  variable A B C X Y Z X' Y' Z' x : set

⋃｛_｝ : ∀ X → ⋃ ｛ X ｝ ≡ X
⋂｛_｝ : ∀ X → ⋂ ｛ X ｝ ≡ X

rep_｛_،_｝≡ : ∀(f : set → set) X Y → ｛ f x ∣ x ∈ ｛ X ، Y ｝ ｝ ≡ ｛ f X ، f Y ｝

∪-comm : A ∪ B ≡ B ∪ A
∩-comm : A ∩ B ≡ B ∩ A

∅-∪ : ∅ ∪ A ≡ A
∅-∩ : ∅ ∩ A ≡ ∅

∪-cancelˡ : A ⊆ B → A ∪ B ≡ B
∪-cancelʳ : A ⊆ B → B ∪ A ≡ B
∩-cancelʳ : A ⊆ B → A ∩ B ≡ A
∩-cancelˡ : A ⊆ B → B ∩ A ≡ A

∪-idemp : A ∪ A ≡ A
∩-idemp : A ∩ A ≡ A

A∩B⊆A : A ∩ B ⊆ A
A∩B⊆B : A ∩ B ⊆ B
A⊆A∪B : A ⊆ A ∪ B
B⊆A∪B : B ⊆ A ∪ B
A-B⊆A : A - B ⊆ A

∪-cong-⊆ : X ⊆ A → Y ⊆ B → X ∪ Y ⊆ A ∪ B
∩-cong-⊆ : X ⊆ A → Y ⊆ B → X ∩ Y ⊆ A ∩ B
\-cong-⊆ : X ⊆ A → Y ⊆ B → X - B ⊆ A - Y
×-cong-⊆ : X ⊆ A → Y ⊆ B → X × Y ⊆ A × B

X⊆A∧X⊆B→X⊆A∩B : X ⊆ A → X ⊆ B → X ⊆ A ∩ B
A⊆X∧B⊆X→A∪B⊆X : A ⊆ X → B ⊆ X → A ∪ B ⊆ X

A⊆B→A∩B≡A : A ⊆ B → A ∩ B ≡ A
A⊆B→B∩A≡A : A ⊆ B → B ∩ A ≡ A

unit-⊆ : ｛ x ｝ ⊆ X ⇔ x ∈ X

disjoint-A-[B-A] : disjoint A (B - A)
A-B≡∅→A⊆B : A - B ≡ ∅ → A ⊆ B
A⊆B→A∪[B-A]≡B : A ⊆ B → A ∪ (B - A) ≡ B

open import Data.Sum as Sum using ([_,_]; [_,_]′)
import Data.Product as Prod
open import Data.Empty

abstract
  ⋃｛ X ｝ = antisym-⊆
    (λ x∈⋃｛X｝ → case from ∈⋃ x∈⋃｛X｝ of λ
      { (X' , X'∈｛X｝ , x∈X') → case from ∈｛ X ｝ X'∈｛X｝ of λ
      { refl → x∈X' }})
    (λ x∈X → to ∈⋃ $ X , to ∈｛ X ｝ refl , x∈X)

  ⋂｛ X ｝ = antisym-⊆
    (λ x∈⋂｛X｝ → case from ∈⋃ $ proj₁ $ from ∈⋂ x∈⋂｛X｝ of λ
      { (X' , X'∈｛X｝ , x∈X') → case from ∈｛ X ｝ X'∈｛X｝ of λ
      { refl → x∈X' }})
    (λ x∈X → to ∈⋂ $ to ∈⋃ (X , to ∈｛ X ｝ refl , x∈X) ,
                     λ X'∈｛X｝ → case from ∈｛ X ｝ X'∈｛X｝ of λ
                       { refl → x∈X })

  rep f ｛ X ، Y ｝≡ = antisym-⊆
    (λ {y} y∈rep → case from ∈r y∈rep of λ { (x , x∈XY , refl) →
      [ to ∈fXfY ∘ inj₁ ∘ cong f , to ∈fXfY ∘ inj₂ ∘ cong f ]′ $ from ∈XY x∈XY})
    (λ {y} y∈fXfY → [ (λ { refl → to ∈r (X , to ∈XY (inj₁ refl) , refl)}) ,
                      (λ { refl → to ∈r (Y , to ∈XY (inj₂ refl) , refl)}) ]′ $
                    from ∈fXfY y∈fXfY)
    where ∈r = ∈fun-rep f
          ∈XY = ∈｛ X ، Y ｝
          ∈fXfY = ∈｛ f X ، f Y ｝
  
  ∪-comm = antisym-⊆ (to ∈∪ ∘ Sum.swap ∘ from ∈∪)
                     (to ∈∪ ∘ Sum.swap ∘ from ∈∪)
  ∩-comm = antisym-⊆ (to ∈∩ ∘ Prod.swap ∘ from ∈∩)
                     (to ∈∩ ∘ Prod.swap ∘ from ∈∩)

  ∅-∪ = antisym-⊆ (Sum.fromInj₂ (⊥-elim ∘ ∈∅) ∘ from ∈∪)(to ∈∪ ∘ inj₂)
  ∅-∩ {A} = antisym-⊆ (proj₁ ∘ from ∈∩) (∅⊆ $ ∅ ∩ A)

  A∩B⊆A = proj₁ ∘ from ∈∩
  A∩B⊆B = proj₂ ∘ from ∈∩
  A⊆A∪B = to ∈∪ ∘ inj₁
  B⊆A∪B = to ∈∪ ∘ inj₂
  A-B⊆A = proj₁ ∘ from ∈-

  ∪-cong-⊆ X⊆A Y⊆B = to ∈∪ ∘ Sum.map X⊆A Y⊆B ∘ from ∈∪ 

  ∩-cong-⊆ X⊆A Y⊆B = to ∈∩ ∘ Prod.map X⊆A Y⊆B ∘ from ∈∩

  \-cong-⊆ X⊆A Y⊆B = to ∈- ∘ Prod.map X⊆A (_∘ Y⊆B) ∘ from ∈-

  ×-cong-⊆ X⊆A Y⊆B z∈X×Y = case from ∈× z∈X×Y of λ
    { (x , x∈X , y , y∈Y , refl) → to ⟨,⟩∈× (X⊆A x∈X , Y⊆B y∈Y)}

  unit-⊆ {x}{X} = mk⇔
    (λ x⊆X → x⊆X (to ∈｛ x ｝ refl))
    (λ x∈X {y} y∈x → subst (_∈ X)(sym $ from ∈｛ x ｝ y∈x) x∈X)

  open import ZF.Reasoning

  X⊆A∧X⊆B→X⊆A∩B {X}{A}{B} X⊆A X⊆B = begin⊆
    X ≡⟨ sym ∩-idemp ⟩
    X ∩ X ⊆⟨ ∩-cong-⊆ X⊆A X⊆B ⟩
    A ∩ B ∎ 
  A⊆X∧B⊆X→A∪B⊆X {A}{X}{B} A⊆X B⊆X = begin⊆
    A ∪ B ⊆⟨ ∪-cong-⊆ A⊆X B⊆X  ⟩
    X ∪ X ≡⟨ ∪-idemp ⟩
    X ∎ 

  A⊆B→A∩B≡A A⊆B = antisym-⊆ A∩B⊆A $ X⊆A∧X⊆B→X⊆A∩B refl-⊆ A⊆B
  A⊆B→B∩A≡A A⊆B = antisym-⊆ A∩B⊆B $ X⊆A∧X⊆B→X⊆A∩B A⊆B refl-⊆

  ∪-cancelˡ A⊆B = antisym-⊆ (A⊆X∧B⊆X→A∪B⊆X A⊆B refl-⊆) B⊆A∪B
  ∩-cancelʳ A⊆B = antisym-⊆ A∩B⊆A (X⊆A∧X⊆B→X⊆A∩B refl-⊆ A⊆B)

  ∪-cancelʳ {A}{B} A⊆B = begin
    B ∪ A ≡⟨ ∪-comm ⟩
    A ∪ B ≡⟨ ∪-cancelˡ A⊆B ⟩
    B ∎
  ∩-cancelˡ {A}{B} A⊆B = begin
    B ∩ A ≡⟨ ∩-comm ⟩
    A ∩ B ≡⟨ ∩-cancelʳ A⊆B ⟩
    A ∎

  ∪-idemp = antisym-⊆ (Sum.[ id , id ] ∘ from ∈∪) (to ∈∪ ∘ inj₁)
  ∩-idemp = antisym-⊆ (proj₁ ∘ from ∈∩) (to ∈∩ ∘ Prod.< id , id >)

  disjoint-A-[B-A] x (x∈A , x∈B-A) = prop (from ∈- x∈B-A) x∈A

  open import ZF.Foundation.Axiom.Nonconstructive
  A-B≡∅→A⊆B {A = A}{B} p {x} x∈A = by-contradiction λ x∉B →
    ∈∅ $ subst (x ∈_) p $ to ∈- $ x∈A , x∉B

  A⊆B→A∪[B-A]≡B {A} A⊆B = antisym-⊆
    ([ A⊆B , proj₁ ∘ from ∈- ] ∘ from ∈∪)
    (λ {x} x∈B → case is? (x ∈ A) of λ
      { (yes x∈A) → to ∈∪ $ inj₁ x∈A
      ; (no x∉A) → to ∈∪ $ inj₂ $ to ∈- $ x∈B , x∉A})
