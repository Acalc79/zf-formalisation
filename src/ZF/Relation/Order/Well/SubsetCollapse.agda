{-# OPTIONS --exact-split #-}
open import ZF.Relation.Order.Well.Base
open import ZF.Relation.Order.Base
open OrderVars hiding (S)

module ZF.Relation.Order.Well.SubsetCollapse (wo : is-well-ordering R X) where

import ZF.Relation.Order.Well.Recursion
module RecDef = ZF.Relation.Order.Well.Recursion

open import ZF.Foundation hiding (module RecDef; _[_∈dom])
open import ZF.Function as F hiding (_∘_; restrict; restrict⊆)
open import ZF.Relation.Base hiding (_∘_)
open import ZF.Relation.Order.Total

open import ZF.Foundation.Axiom.Nonconstructive

wf = proj₁ wo
rel-r = proj₁ (proj₂ wo)
total-o = proj₂ wo

∅-subset-collapse :
  ∃ λ I → I is-initial-segment-of[ X ، R ] ∧ᵈ
    (λ {(I⊆X , _) → isomorphic (restrict R ∅ ∅) ∅ (restrict R I I) I})
∅-subset-collapse =
  ∅ , ∅-initial , isomorphic-refl (total-order-⊆ total-o (∅⊆ X))

module SubsetCollapseCore (Y⊆X : Y ⊆ X)(y∈Y : y ∈ Y) where
  open WellOrdered wo

  f-def : set → set
  f-def f = case is? (nonempty S) of λ
    { (yes nonempty-S) → let open MinDef A-B⊆A nonempty-S in min
    ; (no empty-S) → y}
    where S = X - ran f

  G : set
  G = fun-reify f-def (𝒫 (Y × X))

  ranG⊆X : ran G ⊆ X
  ran-f-def : ∀{f} → f-def f ∈ X

  ranG⊆X {y} y∈ranG with from ∈ran-fun-reify y∈ranG
  ... | _ , _ , refl = ran-f-def

  ran-f-def {f} with  is? (nonempty S)
    where S = X - ran f
  ...  | yes nonempty-S = A-B⊆A min∈
    where open MinDef A-B⊆A nonempty-S
  ... | no empty-S = Y⊆X y∈Y

  <Y = restrict R Y Y
  woY = well-ordered-⊆ Y⊆X
  module WOY = WellOrdered woY

  open RecDef woY {G = G} (∶⟶-cong-⊆ refl ranG⊆X fun-fun-reify)
  I = ran wf-rec

  f = wf-rec

  open Initial X R
  I-init : I is-initial
  inj-f : injective f
  mon-f : monotonic <Y R f

  f-fun = wf-rec-fun
  f-dom≡ = proj₁ f-fun
  f-pfun = proj₂ f-fun
  f-rel : f ∶ Y ⇸ X
  f-rel = proj₂ f-pfun
  f∶Y⟶I = ∶⟶-cong-⊆ (proj₁ f-fun) refl-⊆ $ F.tighten f-pfun
  f| = λ y → F.restrict f (WOY.init y)

  open <-Reasoning
  f-def≡min : ∀{g} →
    let S = X - ran g
    in (nonempty-S : nonempty S)  →
    let open MinDef A-B⊆A nonempty-S
    in f-def g ≡ min
  f-def≡min {g} nonempty-S with is? (nonempty (X - ran g))
  ... | yes _ = min-irrelevant (X - ran g)
  ... | no ¬nonempty-S = ⊥-elim $ ¬nonempty-S nonempty-S
      
  f-nonempty→f≡min : ⋀ y ∈ Y ,
    let S = X - ran (f| y)
    in (nonempty-S : nonempty S)  →
    let open MinDef A-B⊆A nonempty-S
    in f [ y ] ≡ min
  f-nonempty→f≡min {y} y∈Y nonempty-S = begin
    f [ y ] ≡⟨ wf-rec-prop y∈Y ⟩
    G [ f| y ] ≡⟨ valid-fun-reify $ to ∈Rel $ f-rel ∘ restrict⊆ ⟩
    f-def (f| y) ≡⟨ f-def≡min nonempty-S ⟩
    min ∎
    where
    open MinDef A-B⊆A nonempty-S

  f< = λ y → (∀{x} → y < x → f [ y ] < x )
  f-nonempty = λ y → nonempty (X - ran (f| y))

  f-val-prop : ⋀ y ∈ Y , f-nonempty y ∧ f< y
  f-val-prop = (f-nonempty y ∧ f< y)
    by-wf[ proj₁ woY ]-induction-on y step:
    λ {y} y∈Y IH →
    let S = X - ran (f| y)
        y∈S : y ∈ S
        y∈S = to ∈- $ Y⊆X y∈Y , λ y∈ran[f|y] →
          case from ∈ran y∈ran[f|y] of λ { (x , xy∈f|y) →
          case from ⟨,⟩∈restrict xy∈f|y of λ { (xy∈f , x∈init-y , y∈ranf) →
          let x-<Y-y = from WOY.∈init x∈init-y
              x<y = restrict⊆ x-<Y-y
              x∈Y = subst (x ∈_) f-dom≡ $ to ∈dom (y , xy∈f)
              f[x]<y = proj₂ (IH x∈Y x-<Y-y) x<y
              f[x]≡y = proj₂ $ to (f-fun [ x ]≡) xy∈f
          in <-irreflexive f[x]≡y f[x]<y}}
        nonempty-S : nonempty S
        nonempty-S empty-S = empty-S y∈S
    in nonempty-S ,
    λ {x} y<x → let open MinDef A-B⊆A nonempty-S in begin<
      f [ y ] ≡⟨ f-nonempty→f≡min y∈Y nonempty-S ⟩
      min ≤⟨ ≮→≥ (Y⊆X y∈Y) (proj₁ $ from ∈- min∈) $ min-def y∈S ⟩
      y <⟨ y<x ⟩
      x ∎

  open import Function hiding (_⟶_)

  I-init = ran⊆ f-rel ,
    λ {x} x∈I {y} y<x → case from ∈ran x∈I of λ { (x₀ , x₀x∈f) →
      by-contradiction λ y∉I →
      let S = X - ran (f| x₀)
          x₀∈Y = dom⊆ f-rel $ to ∈dom $ x , x₀x∈f
          S-nonempty = proj₁ $ f-val-prop x₀∈Y
          open MinDef A-B⊆A S-nonempty
          y∈X = x<y→x∈X y<x
          y∉ran[f|x₀] = λ y∈ran[f|x₀] → y∉I (ran-cong-⊆ restrict⊆ y∈ran[f|x₀])
      in
      min-def (to ∈- $ y∈X , y∉ran[f|x₀]) $ begin<
        y <⟨ y<x ⟩
        x ≡⟨ sym $ proj₂ $ to (f-fun [ x₀ ]≡) x₀x∈f ⟩
        f [ x₀ ] ≡⟨ f-nonempty→f≡min x₀∈Y S-nonempty ⟩
        min ∎}

  inj-f {x}{x'}{y} xy∈f x'y∈f = case WOY.<-trich x∈Y x'∈Y of λ
    { (inj₁ x<x') → ⊥-elim $ x≮x' f[x]≡f[x'] x<x'
    ; (inj₂ (inj₁ x≡x')) → x≡x'
    ; (inj₂ (inj₂ x'<x)) → ⊥-elim $ x≮x' (sym f[x]≡f[x']) x'<x} 
    where
    x∈Y : x ∈ Y
    x'∈Y : x' ∈ Y
    x∈Y = dom⊆ f-rel $ to ∈dom $ y , xy∈f
    x'∈Y = dom⊆ f-rel $ to ∈dom $ y , x'y∈f
    f[x]≡f[x'] : f [ x ] ≡ f [ x' ]
    f[x]≡f[x'] = begin
      f [ x ] ≡⟨ proj₂ $ to (f-fun [ x ]≡) xy∈f ⟩
      y ≡⟨ sym $ proj₂ $ to (f-fun [ x' ]≡) x'y∈f ⟩
      f [ x' ] ∎
    x≮x' : ∀{x x'} → f [ x ] ≡ f [ x' ] → x WOY.≮ x'
    x≮x' f[x]≡f[x'] x<x' = <-irreflexive f[x]≡f[x'] $ mon-f x<x'

  mon-f' : ∀{x}{x'} → x WOY.< x' → f-def (f| x) < f-def (f| x')
  mon-f' {x}{x'} x<x' with is? (nonempty S) | is? (nonempty S')
    where S = X - ran (f| x)
          S' = X - ran (f| x')
  ... | yes nonempty-S | yes nonempty-S' =
    ⊆-min-< S'⊆S A-B⊆A A-B⊆A nonempty-S' nonempty-S min-S∉S'
    where S = X - ran (f| x)
          S' = X - ran (f| x')
          S'⊆S : S' ⊆ S
          S'⊆S = \-cong-⊆ refl-⊆ $
                 ran-cong-⊆ $
                 restrict-cong-⊆ refl-⊆ (WOY.<-init⊆ x<x') refl-⊆
          module MS = MinDef A-B⊆A nonempty-S
          module MS' = MinDef A-B⊆A nonempty-S'
          min-S∉S' : MS.min ∉ S'
          min-S∉S' minS∈S' =
            let minS≤minS' = ⊆-min-≤ S'⊆S A-B⊆A A-B⊆A nonempty-S' nonempty-S
                minS≡minS' = MS'.≤min→≡min minS∈S' minS≤minS'
                minS∈S' = subst (_∈ S') (sym minS≡minS') MS'.min∈
                minS∉ran[f|x'] = proj₂ $ from ∈- minS∈S'
                x∈init-x' = to WOY.∈init x<x'
                x∈Y = WOY.x<y→x∈X x<x'
            in minS∉ran[f|x'] $ to ∈ran $ x ,
            from (restrict-fun WOY.init⊆ f-fun [ x ]≡) (x∈init-x' ,
            (begin f| x' [ x ] ≡⟨ restrict-to f-fun , WOY.init⊆ [ x∈init-x' ]≡ ⟩
                   f [ x ] ≡⟨ f-nonempty→f≡min x∈Y nonempty-S ⟩
                   MS.min ∎))

  ... | yes _ | no ¬nonempty-S' = ⊥-elim $ ¬nonempty-S' $
    proj₁ $ f-val-prop $ WOY.x<y→y∈X x<x'
  ... | no ¬nonempty-S | _ = ⊥-elim $ ¬nonempty-S $
    proj₁ $ f-val-prop $ WOY.x<y→x∈X x<x'

  mon-f {x}{x'} x<x' = begin<
    f [ x ] ≡⟨ sym $ ≡f[x] $ WOY.x<y→x∈X x<x' ⟩
    f-def (f| x) <⟨ mon-f' x<x' ⟩
    f-def (f| x') ≡⟨ ≡f[x] $ WOY.x<y→y∈X x<x' ⟩
    f [ x' ] ∎
    where ≡f[x] : ⋀ x ∈ Y , f-def (f| x) ≡ f [ x ]
          open import ZF.Reasoning as ≡
            using (begin⊆_; step-⊆) renaming (_∎ to _■)
          ≡f[x] {x} x∈Y = sym $ begin
            f [ x ] ≡⟨ wf-rec-prop x∈Y ⟩
            G [ f| x ]
              ≡⟨ valid-fun-reify $ to ∈𝒫 $ begin⊆
                f| x ⊆⟨ restrict⊆ ⟩
                f ⊆⟨ f-rel ⟩
                Y × X ■ ⟩
            f-def (f| x) ∎

-- Proposition 2.5 (Subset collapse)
subset-collapse :
  let <Y = restrict R Y Y in
  Y ⊆ X → 
  ∃ λ I → I is-initial-segment-of[ X ، R ] ∧ᵈ
          (λ {(I⊆X , _) → isomorphic <Y Y (restrict R I I) I})
subset-collapse {Y = Y} Y⊆X with is? (inhabited Y)
... | yes (y , y∈Y) = I , I-init , f ,
  (f-fun' , fun-inj-surj→bij f-fun' inj-f triv-surj) ,
  λ {x}{x'} x<x' → to ⟨,⟩∈restrict $ mon-f x<x' ,
    to ∈ran (x , f-fun [ WOY.x<y→x∈X x<x' ∈dom]) ,
    to ∈ran (x' , f-fun [ WOY.x<y→y∈X x<x' ∈dom])
  where
  open SubsetCollapseCore Y⊆X y∈Y
  f-fun' : f ∶ Y ⟶ I
  f-fun' = subst (f ∶_⟶ I) f-dom≡ (F.tighten f-pfun)
... | no ¬∃y∈Y =
  case antisym-⊆ (∅⊆ Y) (λ {y} y∈Y → ⊥-elim $ ¬∃y∈Y $ y , y∈Y) of λ
  { refl → ∅-subset-collapse }
