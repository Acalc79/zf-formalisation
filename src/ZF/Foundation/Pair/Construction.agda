{-# OPTIONS --exact-split #-}
module ZF.Foundation.Pair.Construction where

open import ZF.Foundation.Axiom
open import ZF.Foundation.Pair.Base

private
  variable
    A B : set

infixr 22 _∪_ _-_
infixr 23 _∩_

_∪_ _∩_ _-_ : (A B : set) → set
A ∪ B = ⋃ ｛ A ، B ｝
A ∩ B = ⋂ ｛ A ، B ｝

abstract
  A - B = ｛ x ∈ A ∣ x ∉ B ｝

  ∈∪ : ∀{x} → x ∈ A ∪ B ⇔ (x ∈ A ∨ x ∈ B)
  ∈∪ {A}{B} = mk⇔
    (λ x∈A∪B → case from ∈⋃ x∈A∪B of λ
      { (y , y∈｛A,B｝ , x∈y) → case from ∈｛ A ، B ｝ y∈｛A,B｝ of λ
      { (inj₁ refl) → inj₁ x∈y
      ; (inj₂ refl) → inj₂ x∈y} })
    (λ { (inj₁ x∈A) → to ∈⋃ $ A , to ∈｛ A ، B ｝ (inj₁ refl) , x∈A
       ; (inj₂ x∈B) → to ∈⋃ $ B , to ∈｛ A ، B ｝ (inj₂ refl) , x∈B})
  
  ∈∩ : ∀{x} → x ∈ A ∩ B ⇔ (x ∈ A ∧ x ∈ B)
  ∈∩ {A}{B} = mk⇔
    (λ x∈A∩B → case from ∈⋂ x∈A∩B of λ
      { (x∈⋃｛A،B｝ , ∀y∈｛A،B｝,x∈y) →
        ∀y∈｛A،B｝,x∈y (to ∈｛ A ، B ｝ $ inj₁ refl) ,
        ∀y∈｛A،B｝,x∈y (to ∈｛ A ، B ｝ $ inj₂ refl) })
    (λ {(x∈A , x∈B) → to ∈⋂ $
      to ∈∪ (inj₁ x∈A) ,
      λ y∈｛A,B｝ → case from ∈｛ A ، B ｝ y∈｛A,B｝ of λ
      { (inj₁ refl) → x∈A ; (inj₂ refl) → x∈B}})

  ∈- : ∀{x} → x ∈ A - B ⇔ (x ∈ A ∧ x ∉ B)
  ∈- {A}{B} = ∈｛ x ∈ A ∣ x ∉ B ｝

  infixl 24 _×_
  _×_ : (A B : set) → set
  A × B = ｛ x ∈ 𝒫 $ 𝒫 $ A ∪ B ∣ ⋁ a ∈ A , ⋁ b ∈ B , x ≡ ⟨ a ، b ⟩ ｝

  ∈× : ∀{x} → x ∈ A × B ⇔ (⋁ a ∈ A , ⋁ b ∈ B , x ≡ ⟨ a ، b ⟩)
  ∈× {A}{B} = mk⇔ (proj₂ ∘ from ∈｛ x ∈ U ∣ ϕ x ｝)
    (λ { (a , a∈A , b , b∈B , refl) →
      to ∈｛ x ∈ U ∣ ϕ x ｝ $
      to ∈𝒫
        (λ x∈⟨a,b⟩ → case from ∈⟨,⟩ x∈⟨a,b⟩ of λ
        { (inj₁ refl) → to ∈𝒫 $ λ x∈｛a｝ →
        case from ∈｛ a ｝ x∈｛a｝ of λ
        { refl → to ∈∪ $ inj₁ a∈A }
        ; (inj₂ refl) → to ∈𝒫 $ λ x∈｛a,b｝ →
        case from ∈｛ a ، b ｝ x∈｛a,b｝ of λ
        { (inj₁ refl) → to ∈∪ $ inj₁ a∈A
        ; (inj₂ refl) → to ∈∪ $ inj₂ b∈B } }) ,
      a , a∈A , b , b∈B , refl})
    where U = 𝒫 $ 𝒫 $ A ∪ B
          ϕ = λ x → ⋁ a ∈ A , ⋁ b ∈ B , x ≡ ⟨ a ، b ⟩
          -- ∈⟨,⟩ = λ {a}{b}{x} → ∈｛ ｛ a ｝ ، ｛ a ، b ｝ ｝ {x}

  ⟨,⟩∈× : ∀{x y} → ⟨ x ، y ⟩ ∈ A × B ⇔ (x ∈ A ∧ y ∈ B)
  ⟨,⟩∈× {A}{B}{x}{y} = mk⇔
    (λ xy∈A×B → case from ∈× xy∈A×B of λ
    { (x' , x'∈A , y' , y'∈B , xy≡x'y') → case from ⟨,⟩≡ xy≡x'y' of λ
    { (refl , refl) → x'∈A , y'∈B}})
    (λ { (x∈A , y∈B) → to ∈× (x , x∈A , y , y∈B , refl)})
