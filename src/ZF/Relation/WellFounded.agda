{-# OPTIONS --exact-split #-}
module ZF.Relation.WellFounded where

open import ZF.Foundation
open import ZF.Foundation.WellFoundedness public

private
  variable X Y R G RX RY I x y z : set

module _ (rel : R ∈ Rel X X)(wf : well-founded R X) where
  open import ZF.Function hiding (_∘_)
  open import ZF.Relation.Order.Base
  open Notation R

  is-initial-segment : set → Set
  is-initial-segment I = I ⊆ X ∧ (⋀ x ∈ I , ∀{y} → y < x → y ∈ I)

  abstract
    self-initial : is-initial-segment X
    self-initial = refl-⊆ , λ {x} x∈X y<x → dom⊆ rel $ to ∈dom $ x , y<x

  private
    module InitDef x where
      step : set → set
      step x = ｛ y ∈ X ∣ y < x ｝
      open FunRecDef (step x) step public

  init : set → set
  init x = ⋃ (ran f)
    where open InitDef x

  open import Function.Reasoning

  abstract
    init-is-initial : is-initial-segment (init x)
    init-is-initial = {!!}

    init⊆ : is-initial-segment I → x ∈ I → init x ⊆ I
    init⊆ = {!!}
    -- init⊆ {x}{y} y∈init with from ∈⋃ y∈init
    -- ... | S , S∈ran , y∈S with from ∈ran S∈ran
    -- ... | n , nS∈f = let n∈ω = subst (n ∈_) f-dom $ to ∈dom $ S , nS∈f
    --   in case from ∈ω n∈ω of λ
    --   { (inj₁ refl) → case proj₂ $ to (f-is-fun [ 0 ]≡ S) nS∈f of λ
    --     { refl → y∈S ∶ y ∈ f [ 0 ]
    --           |> subst (y ∈_) f[0] ∶ y ∈ ｛ y ∈ X ∣ y < x ｝
    --           |> proj₁ ∘ from ∈｛ y ∈ X ∣ y < x ｝}
    --   ; (inj₂ (m , m∈ω , refl)) →
    --     case proj₂ $ to (f-is-fun [ m ⁺ ]≡ S) nS∈f of λ
    --     { refl → y∈S ∶ y ∈ f [ m ⁺ ]
    --           |> subst (y ∈_) (f[x⁺] m∈ω) ∶ y ∈ ｛ y ∈ X ∣ y < f [ m ] ｝
    --           |> proj₁ ∘ from ∈｛ y ∈ X ∣ y < f [ m ] ｝}}
    --   where open InitDef x

  open ≡-Reasoning

  wf-rec : G ∶ 𝒫 (X × Y) ⟶ Y → set
  wf-rec {G}{Y} p =
    fun-reify {X} ϕ
      λ x∈X → ∃∧!→∃!
        (x∈X |> ∃ (ϕ x) by-wf[ wf ]-induction-on x
          step: {!!})
        (x∈X |> (∀{y y'} → ϕ x y → ϕ x y' → y ≡ y') by-wf[ wf ]-induction-on x
          step: λ { {x} x∈X IH
            (h , (I , I-init , h-fun@(refl , h⇀) , h-val) ,
             x∈dom , refl)
            (h' , (I' , I'-init , h'-fun@(refl , h'⇀) , h'-val) ,
             x∈dom' , refl) →
            begin h [ x ] ≡⟨ h-val x∈dom ⟩
                  G [ restrict h (init x) ] ≡⟨ cong (λ y → G [ y ]) $
                    fun-ext (restrict-is-fun (init⊆ I-init x∈dom) h-fun)
                            (restrict-is-fun (init⊆ I'-init x∈dom') h'-fun)
                    (λ {y} y∈init-x → IH
                      (init⊆ self-initial x∈X y∈init-x)
                      {!proj₂ init-is-initial!}
                      {!!}
                      {!!}) ⟩
                  G [ restrict h' (init x) ] ≡⟨ sym $ h'-val x∈dom' ⟩
                  h' [ x ] ∎})
    -- $ ∃! (ϕ x) by-wf[ wf ]-induction-on x
    --    step: λ x∈X IH → {!!} , {!!} , {!!}
    where ϕ : (x y : set) → Set
          attempt : set → Set
          ϕ x y = ∃ λ h → attempt h ∧ x ∈ dom h ∧ h [ x ] ≡ y
          attempt h = ∃ λ I → is-initial-segment I ∧
            h ∶ I ⟶ Y ∧
            (⋀ x ∈ I , h [ x ] ≡ G [ restrict h (init x) ])

  module _ (G-fun : G ∶ 𝒫 (X × Y) ⟶ Y) where
    private f = wf-rec G-fun
    
    wf-rec-fun : f ∶ X ⟶ Y
    wf-rec-fun = {!!}

    wf-rec-prop : f [ x ] ≡ G [ restrict f (init x) ] 
    wf-rec-prop = {!!}
