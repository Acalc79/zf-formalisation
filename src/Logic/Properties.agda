{-# OPTIONS --safe --exact-split --without-K #-}
module Logic.Properties where

open import Level
open import Function hiding (_⇔_)

open import Logic
open LogicVars

⇔-refl : A ⇔ A
⇔-refl = mk⇔ id id

⇔-sym : A ⇔ B → B ⇔ A
⇔-sym A⇔B = mk⇔ (to A⇔B) (from A⇔B)

⇔-trans : A ⇔ B → B ⇔ C → A ⇔ C
⇔-trans A⇔B B⇔C = mk⇔ (from B⇔C ∘ from A⇔B) (to A⇔B ∘ to B⇔C)

[A→B]→A∧B⇔A : (A → B) → A ∧ B ⇔ A
[A→B]→A∧B⇔A A→B = mk⇔ proj₁ λ a → a , A→B a

[B→A]→A∧B⇔B : (B → A) → A ∧ B ⇔ B
[B→A]→A∧B⇔B B→A = mk⇔ proj₂ λ b → B→A b , b

import Data.Product as Prod
import Data.Sum as Sum

∧-swap : A ∧ B ⇔ B ∧ A
∧-swap = mk⇔ Prod.swap Prod.swap

∧-assoc : (A ∧ B) ∧ C ⇔ A ∧ B ∧ C
∧-assoc = mk⇔ Prod.assocʳ Prod.assocˡ

∧→⇔→→ : (A ∧ B → C) ⇔ (A → B → C)
∧→⇔→→ = mk⇔ Prod.curry Prod.uncurry

→-cond-cong-⇔ : (A → B ⇔ B') → (A → B) ⇔ (A → B')
→-cond-cong-⇔ A→B⇔B' = mk⇔
  (λ A→B a → from (A→B⇔B' a) (A→B a))
  (λ A→B' a → to (A→B⇔B' a) (A→B' a))

∀-⇔-∀′ : (∀ x → P x) ⇔ (∀{x} → P x)
∀-⇔-∀′ = mk⇔ _$- λ-

∀-reform : (f : A → B → C) → (∀{e} → P e → ∃₂ λ x y → e ≡ f x y)
  → --------------------------------------------------------------
  (∀{e} → P e → Q e) ⇔ (∀{x y} → P (f x y) → Q (f x y))
∀-reform f P→e≡fxy = mk⇔
  (λ P→Q {x}{y} Pfxy → P→Q Pfxy)
  (λ Pfxy→Qfxy {e} Pe → case P→e≡fxy Pe of λ { (x , y , refl) → Pfxy→Qfxy Pe})

∃[]→⇔∀[→] : (∃ P → A) ⇔ (∀{x} → P x → A)
∃[]→⇔∀[→] {P}{A} = mk⇔
  (λ ∃P→A {x} Px → ∃P→A $ x , Px)
  λ { ∀P→A (_ , Px) → ∀P→A Px}

∧-cong-⇔ : A ⇔ A' → B ⇔ B' → A ∧ B ⇔ A' ∧ B'
∧-cong-⇔ A⇔A' B⇔B' = mk⇔
  (Prod.map (from A⇔A') (from B⇔B'))
  (Prod.map (to A⇔A') (to B⇔B'))

∧-cond-cong-⇔ : (A → B ⇔ B') → A ∧ B ⇔ A ∧ B'
∧-cond-cong-⇔ A→B⇔B' = mk⇔
  (λ { (a , b) → a , from (A→B⇔B' a) b})
  (λ { (a , b') → a , to (A→B⇔B' a) b'})

∨-cong-⇔ : A ⇔ A' → B ⇔ B' → A ∨ B ⇔ A' ∨ B'
∨-cong-⇔ A⇔A' B⇔B' = mk⇔
  (Sum.map (from A⇔A') (from B⇔B'))
  (Sum.map (to A⇔A') (to B⇔B'))

∃-cong-⇔ : (∀{x} → P x ⇔ P' x) → ∃ P ⇔ ∃ P'
∃-cong-⇔ P⇔P' = mk⇔ (Prod.map₂ (from P⇔P'))(Prod.map₂ (to P⇔P'))

→-cong-⇔ : A ⇔ A' → B ⇔ B' → (A → B) ⇔ (A' → B')
→-cong-⇔ A⇔A' B⇔B' = mk⇔ (λ A→B → from B⇔B' ∘ A→B ∘ to A⇔A')
                          λ A'→B' → to B⇔B' ∘ A'→B' ∘ from A⇔A'

∀-cong-⇔ : (∀{x} → P x ⇔ P' x) → (∀ x → P x) ⇔ (∀ x → P' x)
∀-cong-⇔ P⇔P' = mk⇔ (from P⇔P' ∘_)(to P⇔P' ∘_)

∀′-cong-⇔ : (∀{x} → P x ⇔ P' x) → (∀{x} → P x) ⇔ (∀{x} → P' x)
∀′-cong-⇔ P⇔P' = mk⇔ (λ P {x} → from P⇔P' $ P {x})(λ P' {x} → to P⇔P' $ P' {x})
