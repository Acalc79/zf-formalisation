{-# OPTIONS --exact-split #-}
module ZF.Relation.Order.Total where

open import ZF.Foundation hiding (_[_∈dom])
open import ZF.Relation.Order.Base

is-total-order : (R X : set) → Set
is-total-order R X =
  R ∶ X ⇸ X ∧
  (∀{x} → x ≮ x) ∧
  (∀{x y z} → x < y → y < z → x < z) ∧
  (⋀ x ∈ X , ⋀ y ∈ X , x < y ∨ x ≡ y ∨ y < x)
  where open Notation R

module TotalOrder {R}{X}(total : is-total-order R X) where
  open Relation X (proj₁ total) public
  private variable x y z : set

  <-irrefl : x ≮ x
  <-irrefl = proj₁ $ proj₂ $ total

  <-irreflexive : x ≡ y → x ≮ y
  <-irreflexive refl = <-irrefl

  <-trans : x < y → y < z → x < z
  <-trans = proj₁ $ proj₂ $ proj₂ $ total

  <-trich : ⋀ x ∈ X , ⋀ y ∈ X , x < y ∨ x ≡ y ∨ y < x
  <-trich = proj₂ $ proj₂ $ proj₂ $ total

  <-asym : x < y → y ≮ x
  <-asym = <-irrefl ∘₂ <-trans

  open import Relation.Nullary

  _<?_ : ⋀ x ∈ X , ⋀ y ∈ X , Dec (x < y)
  _<?_ x∈X y∈X with <-trich x∈X y∈X
  ... | inj₁ x<y = yes x<y
  ... | inj₂ (inj₁ refl) = no <-irrefl
  ... | inj₂ (inj₂ y<x) = no $ <-asym y<x

  module <-Reasoning where
    open import Relation.Binary
    open import Relation.Binary.PropositionalEquality
  
    <-strict-poset : StrictPartialOrder _ _ _
    <-strict-poset = record
      { Carrier = set
      ; _≈_ = _≡_
      ; _<_ = _<_
      ; isStrictPartialOrder = record
        { isEquivalence = isEquivalence
        ; irrefl = <-irreflexive
        ; trans = <-trans
        ; <-resp-≈ = resp₂ _<_ } }

    ≮→≥ : ⋀ x ∈ X , ⋀ y ∈ X , (x ≮ y → y ≤ x)
    ≮→≥ {x} x∈X {y} y∈X x≮y = case <-trich x∈X y∈X of λ
      { (inj₁ x<y) → ⊥-elim $ x≮y x<y
      ; (inj₂ (inj₁ refl)) → inj₂ refl
      ; (inj₂ (inj₂ y<x)) → inj₁ y<x}

    ≰→> : ⋀ x ∈ X , ⋀ y ∈ X , (x ≰ y → y < x)
    ≰→> {x} x∈X {y} y∈X x≰y = case <-trich x∈X y∈X of λ
      { (inj₁ x<y) → ⊥-elim $ x≰y $ inj₁ x<y
      ; (inj₂ (inj₁ refl)) → ⊥-elim $ x≰y $ inj₂ refl
      ; (inj₂ (inj₂ y<x)) → y<x}

    open import Relation.Binary.Construct.StrictToNonStrict _≡_ _<_

    ≤-antisym = antisym isEquivalence <-trans <-irreflexive

    open import Relation.Binary.Reasoning.StrictPartialOrderFix <-strict-poset
      as Base public hiding (step-≈; step-≈˘) renaming (
      begin_ to begin≤_;
      begin-equality_ to begin_;
      begin-strict_ to begin<_)

private variable R R' X Y : set

open import ZF.Function as F hiding (id; restrict; restrict⊆; _∘_)
open import Data.Product using (Σ-syntax)

monotonic : (R R' f : set) → Set
monotonic R R' f = ∀{x x'} → x < x' → f [ x ] <' f [ x' ]
  where open Notation R
        open Notation R' renaming (_<_ to _<'_)

open import ZF.Relation.Base hiding (_∘_)

mon-⊆ : ∀{RX RX' RY RY' f} →
  RX' ⊆ RX →
  RY ⊆ RY' →
  monotonic RX RY f
  → --------------------------------------------------
  monotonic RX' RY' f
mon-⊆ RX'⊆RX RY⊆RY' mon-f = RY⊆RY' ∘ mon-f ∘ RX'⊆RX

mon-restrict : ∀{RX RY f} →
  RX ∶ X ⇸ X →
  RY ∶ Y ⇸ Y →
  f ∶ X ⟶ Y →
  monotonic RX RY f
  → --------------------------------------------------
  ∀{X'} → monotonic (restrict RX X' X') RY (F.restrict f X')
mon-restrict {X}{Y}{f = f} RX-rel RY-rel f-fun mon-f {X'}{x}{x'} x<x' =
  subst₂ RY._<_ (sym $ restrict-to f-fun [ RX'.x<y→x∈X x<x' ]≡)
                (sym $ restrict-to f-fun [ RX'.x<y→y∈X x<x' ]≡) $
  mon-f $ restrict⊆ x<x'
  where
  open ≡-Reasoning
  module RY = Relation Y RY-rel
  module RX' = Relation (X ∩ X') (restrict-rel RX-rel)

mon→inj : ∀{RX RY f} →
  is-total-order RX X →
  is-total-order RY Y →
  f ∶ X ⟶ Y →
  monotonic RX RY f →
  injective f
mon→inj {f = f} tot-RX tot-RY f-fun@(refl , _) mon-f =
  from (fun-injective⇔injective f-fun) λ x∈X x'∈X fx≡fx' →
  case RX.<-trich x∈X x'∈X of λ
  { (inj₁ x<x') → ⊥-elim $ RY.<-irreflexive fx≡fx' $ mon-f x<x'
  ; (inj₂ (inj₁ x≡x')) → x≡x'
  ; (inj₂ (inj₂ x'<x)) → ⊥-elim $ RY.<-irreflexive (sym fx≡fx') $ mon-f x'<x }
  where
  module RX = TotalOrder tot-RX
  module RY = TotalOrder tot-RY

mon-bij→⁻¹-mon : ∀{RX RY f} →
  is-total-order RX X →
  is-total-order RY Y →
  (f-fun : f ∶ X ⟶ Y)(mon-f : monotonic RX RY f)(f-bij : bijection f-fun) →
  let f⁻¹ = proj₁ f-bij
  in ----------------------------------------------------------------------
  monotonic RY RX f⁻¹
mon-bij→⁻¹-mon {f = f} RX-tot RY-tot f-fun@(refl , _) mon-f
  bij@(f⁻¹ , hi , bye , f⁻¹-fun@(refl , _)) {y} {y'} y<y' =
  case RX.<-trich (f⁻¹z∈domf y∈domf⁻¹)(f⁻¹z∈domf y'∈domf⁻¹) of λ
  { (inj₁ f⁻¹y<f⁻¹y') → f⁻¹y<f⁻¹y'
  ; (inj₂ (inj₁ f⁻¹y≡f⁻¹y')) → ⊥-elim $
    RY.<-irreflexive (Bf⁻¹.fun-inj y∈domf⁻¹ y'∈domf⁻¹ f⁻¹y≡f⁻¹y') y<y' 
  ; (inj₂ (inj₂ f⁻¹y'<f⁻¹y)) →
    let open RY; open <-Reasoning in ⊥-elim $ <-asym y<y' $
    begin< y' ≡⟨ sym $ Bf.ff⁻¹[y]≡y y'∈domf⁻¹ ⟩
           f [ f⁻¹ [ y' ] ] <⟨ mon-f f⁻¹y'<f⁻¹y ⟩
           f [ f⁻¹ [ y ] ] ≡⟨ Bf.ff⁻¹[y]≡y y∈domf⁻¹ ⟩
           y ∎  }
  where
  module RX = TotalOrder RX-tot
  module RY = TotalOrder RY-tot
  module Bf = WithBijection f-fun bij
  module Bf⁻¹ = WithBijection f⁻¹-fun Bf.bij⁻¹
  y∈domf⁻¹ : y ∈ dom f⁻¹
  y'∈domf⁻¹ : y' ∈ dom f⁻¹
  y∈domf⁻¹ = RY.x<y→x∈X y<y'
  y'∈domf⁻¹ = RY.x<y→y∈X y<y'
  f⁻¹z∈domf : ⋀ z ∈ dom f⁻¹ , f⁻¹ [ z ] ∈ dom f
  f⁻¹z∈domf {z} z∈domf⁻¹ =
    subst (f⁻¹ [ z ] ∈_) Bf⁻¹.ran≡ $
    to ∈ran $ z , f⁻¹-fun [ z∈domf⁻¹ ∈dom]

isomorphic : (R R' X Y : set) → Set
isomorphic R X R' Y =
  ∃ λ f → (Σ[ p ∈ f ∶ X ⟶ Y ] bijection p) ∧ monotonic R R' f

isomorphic-refl : (ordR : is-total-order R X) → isomorphic R X R X
isomorphic-refl {R}{X} ordR =
  F.id X ,
  (id-fun , biejction-id) ,
  λ x<x' → subst₂ _<_ (sym $ id[]≡ $ x<y→x∈X x<x')(sym $ id[]≡ $ x<y→y∈X x<x') x<x'
  where open TotalOrder ordR

-- Definition (Initial segment)
_is-initial-segment-of[_,_] : (I X R : set) → Set
I is-initial-segment-of[ X , R ] =
  I ⊆ X ∧ (⋀ x ∈ I , ∀{y} → y < x → y ∈ I)
  where open Notation R

module Initial (X R : set) where
  _is-initial : (I : set) → Set
  I is-initial = I is-initial-segment-of[ X , R ]

module _ {X R} where
  open Initial X R

  ∅-initial : ∅ is-initial
  ∅-initial = ∅⊆ X , ⊥-elim ∘ ∈∅

  self-initial : R ∶ X ⇸ X → X is-initial
  self-initial R-rel = refl-⊆ , λ _ → x<y→x∈X
    where open Relation X R-rel

  module _ {I I'}(init-I : I is-initial)(init-I' : I' is-initial) where
    I⊆X = proj₁ init-I
    y<x→y∈I = proj₂ init-I
    I'⊆X = proj₁ init-I'
    y<x→y∈I' = proj₂ init-I'

    ∩-initial : (I ∩ I') is-initial
    ∩-initial = I⊆X ∘ A∩B⊆A , λ {x} x∈I∩I' {y} y<x →
      to ∈∩ (y<x→y∈I (proj₁ $ from ∈∩ x∈I∩I') y<x ,
             y<x→y∈I' (proj₂ $ from ∈∩ x∈I∩I') y<x)

    ∪-initial : (I ∪ I') is-initial
    ∪-initial = A⊆X∧B⊆X→A∪B⊆X I⊆X I'⊆X ,
      λ {x} x∈I∪I' {y} y<x →
      to ∈∪ $
      map (λ x∈I → y<x→y∈I x∈I y<x)(λ x∈I' → y<x→y∈I' x∈I' y<x) $
      from ∈∪ x∈I∪I'
      where open import Data.Sum

abstract
  total-order-⊆ :
    is-total-order R X →
    Y ⊆ X
    → ----------------------------------------
    is-total-order (restrict R Y Y) Y
  total-order-⊆ {Y = Y} total@(R-rel , _) Y⊆X =
    rel-restrict ,
    <-irrefl ∘ R'⊆ ,
    (λ x<y y<z → to ⟨,⟩∈restrict $
      <-trans (R'⊆ x<y )(R'⊆ y<z) ,
      x<y→x∈X x<y , x<y→y∈X y<z) ,
    λ x∈Y y∈Y → case <-trich (Y⊆X x∈Y)(Y⊆X y∈Y) of λ
      { (inj₁ x<y) → inj₁ $ to ⟨,⟩∈restrict $ x<y , x∈Y , y∈Y
      ; (inj₂ (inj₁ refl)) → inj₂ (inj₁ refl)
      ; (inj₂ (inj₂ y<x)) → inj₂ $ inj₂ $ to ⟨,⟩∈restrict $ y<x , y∈Y , x∈Y }
    where R'⊆ = restrict⊆
          rel-restrict = ∶⇸-cong-⊆ A∩B⊆B A∩B⊆B (restrict-rel R-rel)
          open import Data.Sum using (map; map₂)
          open TotalOrder total hiding (x<y→x∈X; x<y→y∈X)
          open Relation Y rel-restrict

{-
ran-init : ∀{X Y RX RY X' f} →
  is-total-order RX X →
  is-total-order RY Y →
  f ∶ X' ⟶ Y →
  monotonic RX RY f →
  X' is-initial-segment-of[ X , RX ]
  → -------------------------------------------------------
  ran f is-initial-segment-of[ Y , RY ]
ran-init {Y = Y}{RX}{RY}{X'}{f}
  tot-RX@(RX-rel , _) tot-RY@(RY-rel , _) f-fun@(refl , f-pfun@(_ , f-rel))
  mon-f (X'⊆X , ∀x'<x→x'∈X') =
  ran⊆ f-rel , λ {y} y∈ranf {y'} y'<y →
  case from ∈ran y∈ranf of λ
    { (x , xy∈f) → {!!} }
    -- let x' = f ⁻¹ [ y' ]
    --     y'≡f[x'] = sym $ f[f⁻¹[y]]≡y y∈ranf
    -- in to ∈ran $ x' , from (f-fun [ x' ]≡) ({!!} , {!f[f⁻¹[y]]≡y y∈ranf!})}
  where
  open ≡-Reasoning
  open TotalOrder tot-RY
  RX' = restrict RX X' X'
  tot-RX' = total-order-⊆ tot-RX X'⊆X
  mon-f' : monotonic RX' RY f
  mon-f' = mon-⊆ restrict⊆ refl-⊆ mon-f
  inj-f : injective f
  inj-f = mon→inj tot-RX' tot-RY f-fun mon-f'
  open Bijection (F.tighten f-pfun) inj-f triv-surj
-}
