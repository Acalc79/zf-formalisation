{-# OPTIONS --exact-split #-}
module ZF.Foundation.Axiom.Choice.Properties where

open import ZF.Foundation
open import ZF.Function
open import ZF.Foundation.Axiom.Choice.Base

choice : ∀ F (p : ⋀ x ∈ F , nonempty x ∧ (⋀ y ∈ F , (x ≢ y → disjoint x y))) → set
choice F p = ｛ x ∈ elem $ choice-exists F p ∣ (x ∈ ⋃ F) ｝

open import Logic.Properties

abstract
  ∈choice : ∀{F}{p : ⋀ x ∈ F , nonempty x ∧ (⋀ y ∈ F , (x ≢ y → disjoint x y))}
    → ----------------------------------------------------------------------------
    ∀{x} → x ∈ choice F p ⇔ (x ∈ elem (choice-exists F p) ∧ (⋁ X ∈ F , x ∈ X))
  ∈choice {F}{p}{x} = begin
    x ∈ choice F p ⇔⟨ ∈｛ x ∈ c ∣ (x ∈ ⋃ F) ｝ ⟩
    (x ∈ c ∧ x ∈ ⋃ F) ⇔⟨ ∧-cong-⇔ ⇔-refl ∈⋃ ⟩
    (x ∈ c ∧ (⋁ X ∈ F , x ∈ X)) ∎
    where
    c = elem $ choice-exists F p
    open import Function.Equivalence.Reasoning

  choice-prop : ∀{F}(p : ⋀ x ∈ F , nonempty x ∧ (⋀ y ∈ F , (x ≢ y → disjoint x y)))
    → --------------------------------------------------------------------------------
    ⋀ A ∈ F , ∃! λ z → z ∈ choice F p ∧ z ∈ A
  choice-prop {F} p {A} A∈F
    with (z , (z∈c , z∈A) , !z) ← prop (choice-exists F p) A∈F =
    z , (to ∈choice (z∈c , A , A∈F , z∈A) , z∈A) ,
    λ { (y∈c , y∈A) → !z $ proj₁ (from ∈choice y∈c) , y∈A}

  open import ZF.Reasoning

  choice-fun : ∀{X} → ∅ ∉ X → ∃ λ f → f ∶ X ⟶ ⋃ X ∧ (⋀ A ∈ X , f [ A ] ∈ A)
  choice-fun {X} ∅∉X = f , f-fun , ∀x∈X→f[x]∈x
    where tag = λ A → ｛ ⟨ A ، x ⟩ ∣ x ∈ A ｝
          F = ｛ tag A ∣ A ∈ X ｝
          ∅∉F : ⋀ x ∈ F , nonempty x
          ∅∉F x'∈F empty-x' with (x , x∈X , refl) ← from (∈fun-rep tag) x'∈F =
            let x≡∅ : x ≡ ∅
                x≡∅ = ∉X→X≡∅ λ {y} y∈x → empty-x' $ to (∈fun-rep ⟨ x ،_⟩) $
                  y , y∈x , refl
            in ∅∉X $ subst (_∈ X) x≡∅ x∈X
          ∀disjoint : ⋀ x ∈ F , ⋀ y ∈ F , (x ≢ y → disjoint x y)
          ∀disjoint x'∈F y'∈F x'≢y' z (z∈x' , z∈y')
            with (x , x∈X , refl) ← from (∈fun-rep tag) x'∈F
               | (y , y∈X , refl) ← from (∈fun-rep tag) y'∈F
            with (a , a∈x , refl) ← from (∈fun-rep ⟨ x ،_⟩) z∈x'
               | (b , b∈y , yb≡xa) ← from (∈fun-rep ⟨ y ،_⟩) z∈y' =
               x'≢y' $ cong tag $ sym $ proj₁ $ from ⟨,⟩≡ yb≡xa
          p : ⋀ x ∈ F , nonempty x ∧ (⋀ y ∈ F , (x ≢ y → disjoint x y))
          p X∈F = ∅∉F X∈F , ∀disjoint X∈F
          f = choice F p
          f-prop = choice-prop p
          f-fun : f ∶ X ⟶ ⋃ X
          f-pfun : f ∶ X ⇀ ⋃ X
          f-rel : f ∶ X ⇸ ⋃ X
          f-rel {x} x∈f = case proj₂ $ from ∈choice x∈f of λ
            { (A' , A'∈F , x∈A') → case from (∈fun-rep tag) A'∈F of λ
            { (A , A∈X , refl) → case from (∈fun-rep ⟨ A ،_⟩) x∈A' of λ
            { (x' , x'∈A , refl) → to ⟨,⟩∈× $ A∈X , to ∈⋃ (A , A∈X , x'∈A)}}}
          f-pfun = (λ {x}{y}{y'} xy∈f xy'∈f →
            case from ∈choice xy∈f , from ∈choice xy'∈f of λ
            { ((xy∈c , A , A∈F , xy∈A) , xy'∈c , A' , A'∈F , xy'∈A') →
              case from (∈fun-rep tag) A∈F , from (∈fun-rep tag) A'∈F of λ
            { ((B , B∈X , refl) , B' , B'∈X , refl) →
              case from (∈fun-rep ⟨ B ،_⟩) xy∈A , from (∈fun-rep ⟨ B' ،_⟩) xy'∈A' of λ
            { ((z , z∈B , Bz≡xy) , z' , z'∈B' , B'z'≡xy') →
              case from ⟨,⟩≡ Bz≡xy , from ⟨,⟩≡ B'z'≡xy' of λ
            { ((refl , refl) , refl , refl) → case f-prop A∈F of λ
            { (k , (k∈c , k∈A) , !k) →
              let xy≡xy' : ⟨ x ، y ⟩ ≡ ⟨ x ، y' ⟩
                  xy≡xy' = begin
                    ⟨ x ، y ⟩
                      ≡⟨ sym $ !k $ to ∈choice (xy∈c , A , A∈F , xy∈A) , xy∈A ⟩
                    k ≡⟨ !k $ to ∈choice (xy'∈c , A' , A'∈F , xy'∈A') , xy'∈A' ⟩
                    ⟨ x ، y' ⟩ ∎
              in proj₂ $ from ⟨,⟩≡ xy≡xy' }}}}}) ,
            f-rel
          f-fun = antisym-⊆ (dom⊆ f-rel)
            (λ {x} x∈X → case f-prop $ to (∈fun-rep tag) $ x , x∈X , refl of λ
              { (p , (p∈f , p∈tagx) , _) → case from (∈fun-rep ⟨ x ،_⟩) p∈tagx of λ
              { (a , a∈x , refl) → to ∈dom $ a , p∈f}} ) ,
            f-pfun
          ∀x∈X→f[x]∈x : ⋀ A ∈ X , f [ A ] ∈ A
          ∀x∈X→f[x]∈x {A} A∈X =
            let A'∈F : tag A ∈ F
                A'∈F = to (∈fun-rep tag) (A , A∈X , refl) in
            case f-prop A'∈F of λ { (z , (z∈f , z∈A') , _) → 
            case from (∈fun-rep ⟨ A ،_⟩) z∈A' of λ { (a , a∈A , refl) →
            case proj₂ $ to (f-fun [ A ]≡) z∈f of λ { refl → a∈A }}}
