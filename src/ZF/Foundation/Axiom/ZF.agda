{-# OPTIONS --exact-split #-}
module ZF.Foundation.Axiom.ZF where

open import ZF.Foundation.Axiom.Base

r-prop = (x y : set) → Set

is-func : (ϕ : r-prop)(X : set) → Set
is-func ϕ X = ⋀ x ∈ X , ∃! λ y → ϕ x y

postulate
  set-ext : ∀ X Y (p : ∀ z → z ∈ X ⇔ z ∈ Y) → X ≡ Y

  ∞-exists : ∃ λ ∞ →
    (∀ ∅ → empty ∅ → ∅ ∈ ∞) ∧
    (⋀ x ∈ ∞ , ∀ s[x] (q : s[x] =S[ x ]) → s[x] ∈ ∞)

  ⋃-exists : ∀ X → ∃[ ⋃X ] (⋀ y ∈ X , ⋀ z ∈ y , z ∈ ⋃X)

  𝒫-exists : ∀ X → ∃[ 𝒫[X] ] ∀ Z → (p : Z ⊆ X) → Z ∈ 𝒫[X]

  replacement-exists : (ϕ : r-prop) →
    ∀ X (p : is-func ϕ X)
    → ----------------------------------------
    ∃[ Y ] (⋀ x ∈ X , ⋁ y ∈ Y , ϕ x y)

  separation : (ϕ : set → Set) → ∀ X → ∃[ Y ] ∀{x} → x ∈ Y ⇔ (x ∈ X ∧ ϕ x)

separation-syntax : (ϕ : set → Set)(X : set) → set
separation-syntax ϕ = elem ∘ separation ϕ

syntax separation-syntax (λ x → ϕ) X = ｛ x ∈ X ∣ ϕ ｝

abstract
  separation-prop-syntax : (ϕ : set → Set)(X : set) →
    ∀{x} → x ∈ ｛ x ∈ X ∣ ϕ x ｝ ⇔ (x ∈ X ∧ ϕ x)
  separation-prop-syntax ϕ = prop ∘ separation ϕ

syntax separation-prop-syntax (λ x → ϕ) X = ∈｛ x ∈ X ∣ ϕ ｝

∅ : set
∅ = ｛ x ∈ elem ∞-exists ∣ x ≢ x ｝

𝒫 : (X : set) → set
𝒫 X = ｛ x ∈ elem $ 𝒫-exists X ∣ x ⊆ X ｝

abstract
  ∈𝒫 : ∀{X x} → x ∈ 𝒫 X ⇔ x ⊆ X
  ∈𝒫 {X} {x} =
    mk⇔ (proj₂ ∘ from 𝒫-def)
        λ x⊆X → to 𝒫-def $ prop (𝒫-exists X) x x⊆X , x⊆X
    where 𝒫-def = ∈｛ x ∈ elem $ 𝒫-exists X ∣ x ⊆ X ｝


⋃ : (X : set) → set
⋃ X = ｛ x ∈ elem $ ⋃-exists X ∣ ⋁ y ∈ X , x ∈ y ｝

abstract
  ∈⋃ : ∀{X x} → x ∈ ⋃ X ⇔ (⋁ Y ∈ X , x ∈ Y)
  ∈⋃ {X} {x} = mk⇔ (proj₂ ∘ from ⋃-def)
    λ { p@(y , y∈X , x∈y) → to ⋃-def $ prop (⋃-exists X) y∈X x∈y  , p}
    where ⋃-def = ∈｛ x ∈ elem $ ⋃-exists X ∣ ⋁ y ∈ X , x ∈ y ｝

replacement : ∀(ϕ : r-prop){X}(p : ⋀ x ∈ X , ∃! (ϕ x)) → set
replacement ϕ {X} p =
  ｛ y ∈ elem $ replacement-exists ϕ X p ∣ ⋁ x ∈ X , ϕ x y ｝

private
  rfs-ϕ : ∀ X (f : ∀ x → x ∈ X → set) → r-prop
  rfs-p : ∀ X
    (f : ∀ x → x ∈ X → set)
    (p : ∀{x}(q q' : x ∈ X) → f x q ≡ f x q')
    → --------------------------------------------------
    ⋀ x ∈ X , (∃! λ y → rfs-ϕ X f x y)

rfs-ϕ X f x y = x ∈ X ∧ᵈ λ x∈X → f x x∈X ≡ y
rfs-p X f p {x} x∈X = f x x∈X , (x∈X , refl) , λ { (x∈X₁ , refl) → p x∈X x∈X₁}

replacement-fun-syntax : ∀ X
  (f : ∀ x → x ∈ X → set)
  (p : ∀{x}(q q' : x ∈ X) → f x q ≡ f x q') →
  set
replacement-fun-syntax X f p = replacement (rfs-ϕ X f)(rfs-p X f p)

replacement-fun-syntax' : ∀ X (f : set → set) → set
replacement-fun-syntax' X f = replacement-fun-syntax X (λ x _ → f x) λ _ _ → refl

syntax replacement-fun-syntax X (λ x → y) p = ｛[ y ]∣ x ∈ X ｝[ p ]
syntax replacement-fun-syntax' X (λ x → y) = ｛ y ∣ x ∈ X ｝

abstract
  ∈replacement : 
    (ϕ : r-prop)
    {X : set}
    (p : ⋀ x ∈ X , ∃! λ y → ϕ x y)
    → ----------------------------------------
    ∀{y} → y ∈ replacement ϕ p ⇔ (⋁ x ∈ X , ϕ x y)
  ∈replacement ϕ {X} p {y} =
    mk⇔ (proj₂ ∘ from replacement-def)
        λ { q@(x , x∈X , ϕXxy) → to replacement-def $
        (case prop r x∈X of λ { (y' , y'∈r , ϕXxy') →
         subst (_∈ elem r) (
           begin y' ≡⟨ sym $ ϕXxy' |> proj₂ (proj₂ $ p x∈X) ⟩
                 proj₁ (p x∈X) ≡⟨ ϕXxy |> proj₂ (proj₂ $ p x∈X) ⟩
                 y
           ∎) y'∈r}) ,
         q }
    where r = replacement-exists ϕ X p
          replacement-def = ∈｛ y ∈ elem r ∣ ⋁ x ∈ X , ϕ x y ｝
          open ≡-Reasoning

  ∈fun-replacement' :
    ∀{X}
    (f : ∀ x → x ∈ X → set)
    (p : ∀{x}(q q' : x ∈ X) → f x q ≡ f x q')
    → -------------------------------------------------
    ∀{y} → y ∈ ｛[ f x ]∣ x ∈ X ｝[ p ] ⇔ (∃ λ x → x ∈ X ∧ᵈ λ x∈X → f x x∈X ≡ y)
  ∈fun-replacement' {X = X} f p {y} = mk⇔
    (λ y∈rep → case from ∈rep y∈rep of λ
      { (x , x∈X , x∈X₁ , refl) → x , x∈X , p x∈X x∈X₁})
    λ { (x , x∈X , refl) → to ∈rep (x , x∈X , x∈X , refl)}
    where ∈rep = ∈replacement (rfs-ϕ X f)(rfs-p X f p)

  ∈fun-rep :
    ∀{X}(f : set → set)
    → -------------------------------------------------
    ∀{y} → y ∈ ｛ f x ∣ x ∈ X ｝ ⇔ (⋁ x ∈ X , f x ≡ y)
  ∈fun-rep {X = X} f = ∈fun-replacement' {X = X}(λ x _ → f x) λ _ _ → refl

⋂ : (X : set) → set
⋂ X = ｛ x ∈ ⋃ X ∣ ⋀ y ∈ X , x ∈ y ｝

abstract
  ∈⋂ : ∀{X x} → x ∈ ⋂ X ⇔ (x ∈ ⋃ X ∧ (⋀ Y ∈ X , x ∈ Y))
  ∈⋂ {X} {x} = ∈｛ x ∈ ⋃ X ∣ ⋀ y ∈ X , x ∈ y ｝
