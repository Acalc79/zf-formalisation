{-# OPTIONS --exact-split #-}
module ZF.Foundation.Axiom.Properties where

open import ZF.Foundation.Axiom.Base
open import ZF.Foundation.Axiom.ZF

private
  variable
    X Y Z W : set

abstract
  antisym-⊆ : X ⊆ Y → Y ⊆ X → X ≡ Y
  antisym-⊆ {X}{Y} X⊆Y Y⊆X = set-ext X Y λ z → mk⇔ X⊆Y Y⊆X

  refl-⊆ : X ⊆ X
  refl-⊆ = id

  reflexive-⊆ : X ≡ Y → X ⊆ Y
  reflexive-⊆ refl = refl-⊆

  trans-⊆ : X ⊆ Y → Y ⊆ Z → X ⊆ Z
  trans-⊆ X⊆Y Y⊆Z = Y⊆Z ∘ X⊆Y

  ∈∅ : ∀{x} → x ∉ ∅
  ∈∅ {x} x∈∅ =
    proj₂ (from (prop $ separation (λ x → x ≢ x) $ elem ∞-exists) x∈∅) refl

  ∅⊆ : (X : set) → ∅ ⊆ X
  ∅⊆ X {x} x∈∅ with () ← ∈∅ x∈∅

  ∉X→X≡∅ : (∀ {x} → x ∉ X) → X ≡ ∅
  ∉X→X≡∅ {X} x∉X = antisym-⊆ (⊥-elim ∘ x∉X) (∅⊆ X)

  ¬inhabited≡∅ : ¬ inhabited X → X ≡ ∅
  ¬inhabited≡∅ {X} ¬∃x∈X = ∉X→X≡∅ λ {x} x∈X → ¬∃x∈X (x , x∈X)

  X⊆∅→X≡∅ : X ⊆ ∅ → X ≡ ∅
  X⊆∅→X≡∅ {X} X⊆∅ = antisym-⊆ X⊆∅ $ ∅⊆ X

  x∈𝒫[_] : ∀ x → x ∈ 𝒫 x
  x∈𝒫[ x ] = to ∈𝒫 refl-⊆

  ∈𝒫∅ : ∀ {x} → x ∈ 𝒫 ∅ → x ≡ ∅
  ∈𝒫∅ p = antisym-⊆ (from ∈𝒫 p) (∅⊆ _)

  sep⊆ : (ϕ : set → Set)(X : set)
    → ----------------------------------------
    ｛ x ∈ X ∣ ϕ x ｝ ⊆ X
  sep⊆ ϕ X x∈sep = proj₁ $ from ∈｛ x ∈ X ∣ ϕ x ｝ x∈sep

  open import Data.Empty

  ⋃∅ : ⋃ ∅ ≡ ∅
  ⋃∅ = antisym-⊆ (⊥-elim ∘ ∈∅ ∘ proj₁ ∘ proj₂ ∘ from ∈⋃) (∅⊆ $ ⋃ ∅)

  sep∅ : (ϕ : set → Set) → ｛ x ∈ ∅ ∣ ϕ x ｝ ≡ ∅
  sep∅ ϕ = antisym-⊆ (sep⊆ ϕ ∅) (∅⊆ ｛ x ∈ ∅ ∣ ϕ x ｝)

  ⋂∅ : ⋂ ∅ ≡ ∅
  ⋂∅ = begin ⋂ ∅              ≡⟨ cong (λ X → ｛ x ∈ X ∣ ϕ x ｝) ⋃∅ ⟩
            ｛ x ∈ ∅ ∣ ϕ x ｝ ≡⟨ sep∅ ϕ ⟩
             ∅
       ∎
    where ϕ = λ x → ⋀ y ∈ ∅ , x ∈ y
          open ≡-Reasoning

  open import Logic.Properties
  
  ⊆-⋃ : X ∈ Y → X ⊆ ⋃ Y
  ⊆-⋃ {X} X∈Y x∈X = to ∈⋃ $ X , X∈Y , x∈X

  ⋃-⊆ : (⋀ x ∈ X , x ⊆ Y) → ⋃ X ⊆ Y
  ⋃-⊆ ∀x⊆Y {a} a∈⋃X = case from ∈⋃ a∈⋃X of λ { (x , x∈X , a∈x) →
    ∀x⊆Y x∈X a∈x}

  sep-⇔ : ∀{X}{ϕ ψ : set → Set}
    (p : ∀{x} → ϕ x ⇔ ψ x)
    → -------------------------------------------------------
    ｛ x ∈ X ∣ ψ x ｝ ≡ ｛ x ∈ X ∣ ϕ x ｝
  sep-⇔ {X}{ϕ}{ψ} p = antisym-⊆
    (to ∈｛ x ∈ X ∣ ϕ x ｝ ∘ to (∧-cong-⇔ ⇔-refl p) ∘ from ∈｛ x ∈ X ∣ ψ x ｝)
    (to ∈｛ x ∈ X ∣ ψ x ｝ ∘ to (∧-cong-⇔ ⇔-refl (⇔-sym p)) ∘ from ∈｛ x ∈ X ∣ ϕ x ｝)

  sep-sep : ∀{X}{ϕ ψ : set → Set}
    → -------------------------------------------------------
    ｛ x ∈ ｛ x ∈ X ∣ ϕ x ｝ ∣ ψ x ｝ ≡ ｛ x ∈ X ∣ ϕ x ∧ ψ x ｝
  sep-sep {X}{ϕ}{ψ} = antisym-⊆
    (λ x∈sepψ → case from ∈sepψ x∈sepψ of λ { (x∈sepϕ , ψx) →
      case from ∈sepϕ x∈sepϕ of λ { (x∈X , ϕx) → to ∈sep∧ $
      x∈X , ϕx , ψx}})
    λ x∈sep∧ → case from ∈sep∧ x∈sep∧ of λ { (x∈X , ϕx , ψx) →
      to ∈sepψ $ to ∈sepϕ (x∈X , ϕx) , ψx}
    where ∈sepϕ = ∈｛ x ∈ X ∣ ϕ x ｝
          ∈sepψ = ∈｛ x ∈ ｛ x ∈ X ∣ ϕ x ｝ ∣ ψ x ｝
          ∈sep∧ = ∈｛ x ∈ X ∣ ϕ x ∧ ψ x ｝

  rep-rep : ∀{X}(g f : set → set)
    → -------------------------------------------------------
    ｛ g y ∣ y ∈ ｛ f x ∣ x ∈ X ｝ ｝ ≡ ｛ g (f x) ∣ x ∈ X ｝
  rep-rep g f = antisym-⊆
    (λ z∈rep-rep → case from (∈fun-rep g) z∈rep-rep of λ
      { (y , y∈rep , refl) → case from (∈fun-rep f) y∈rep of λ
      { (x , x∈X , refl) → to (∈fun-rep $ g ∘ f) (x , x∈X , refl)}})
    (λ z∈rep → case from (∈fun-rep $ g ∘ f) z∈rep of λ
      { (x , x∈X , refl) → to (∈fun-rep g) $
        f x , to (∈fun-rep f) (x , x∈X , refl) , refl})

  import Data.Product as Prod

  𝒫-cong-⊆ : X ⊆ Y → 𝒫 X ⊆ 𝒫 Y
  𝒫-cong-⊆ X⊆Y x∈𝒫X = to ∈𝒫 $ X⊆Y ∘ from ∈𝒫 x∈𝒫X

  ⋃-cong-⊆ : X ⊆ Y → ⋃ X ⊆ ⋃ Y
  ⋃-cong-⊆ X⊆Y = to ∈⋃ ∘ Prod.map₂ (Prod.map₁ X⊆Y) ∘ from ∈⋃

  sep-cong-⊆ : ∀{ϕ ψ : set → Set} →
    X ⊆ Y → (∀{x} → ϕ x → ψ x)
    → --------------------------------------------------
    ｛ x ∈ X ∣ ϕ x  ｝ ⊆ ｛ x ∈ Y ∣ ψ x  ｝
  sep-cong-⊆ {X}{Y}{ϕ}{ψ} X⊆Y ϕ→ψ =
    to ∈｛ x ∈ Y ∣ ψ x ｝ ∘ Prod.map X⊆Y ϕ→ψ ∘ from ∈｛ x ∈ X ∣ ϕ x ｝

  rep-cong-⊆ : ∀{f : set → set} →
    X ⊆ Y
    → --------------------------------------------------
    ｛ f x ∣ x ∈ X  ｝ ⊆ ｛ f x ∣ x ∈ Y  ｝
  rep-cong-⊆ {X}{Y}{f} X⊆Y =
    to (∈fun-rep f) ∘ Prod.map₂ (Prod.map₁ X⊆Y) ∘ from (∈fun-rep f)

