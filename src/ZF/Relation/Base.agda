{-# OPTIONS --exact-split #-}
module ZF.Relation.Base where

open import ZF.Foundation as F hiding (id; _∘_)

private variable A X Y Z P Q R R' S T X' Y' x y z e : set

id : set → set
id X = reify X X _≡_

private
  θ-∘ : (P Q x y : set) → Set
θ-∘ P Q x y = ∃ λ z → ⟨ x ، z ⟩ ∈ P ∧ ⟨ z ، y ⟩ ∈ Q

infixr 25 _∘_
_∘_ : (P Q : set) → set
Q ∘ P = reify (dom P)(ran Q)(θ-∘ P Q)

abstract
  rel-ext : R ∶ X ⇸ Y → R' ∶ X' ⇸ Y' →
    (∀{x y} → ⟨ x ، y ⟩ ∈ R ⇔ ⟨ x ، y ⟩ ∈ R')
    → ------------------------------------------------------------
    R ≡ R'
  rel-ext {R}{R' = R'} R-rel R'-rel ext = set-ext R R'
    (λ z → mk⇔ (λ z∈R → case from ∈× $ R-rel z∈R of λ
                  { (_ , _ , _ , _ , refl) → from ext z∈R})
               (λ z∈R' → case from ∈× $ R'-rel z∈R' of λ
                  { (_ , _ , _ , _ , refl) → to ext z∈R'}))

  tighten : R ∶ X ⇸ Y → R ∶ dom R ⇸ ran R
  tighten r-rel e∈R =
    case from ∈× $ r-rel e∈R of λ{ (x , _ , y , _ , refl) →
    to ⟨,⟩∈× $ to ∈dom (y , e∈R) , to ∈ran (x , e∈R)}

  -- identity and composition

  id-rel : id X ∶ X ⇸ X
  id-rel = reify-rel

  ∘-rel : Q ∶ Y ⇸ Z → P ∶ X ⇸ Y →  Q ∘ P ∶ X ⇸ Z
  ∘-rel Q-rel P-rel =
    ∶⇸-cong-⊆ (dom⊆ P-rel) (ran⊆ Q-rel) reify-rel

  import Data.Product as Prod
  open import Logic.Properties
  open import Function.Equivalence.Reasoning
  
  ∈id : ∀{X e} → e ∈ id X ⇔ ∃ λ x → e ≡ ⟨ x ، x ⟩ ∧ x ∈ X
  ∈id {X}{e} = begin
    e ∈ id X ⇔⟨ ∈reify ⟩
    (∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ x ∈ X ∧ y ∈ X ∧ x ≡ y) ⇔⟨ ∃-cong-⇔ $ mk⇔
      (λ {(_ , refl , x∈X , _ , refl) → refl , x∈X})
      (λ {(refl , x∈X) → _ , refl , x∈X , x∈X , refl}) ⟩
    (∃ λ x → e ≡ ⟨ x ، x ⟩ ∧ x ∈ X) ∎

  ⟨,⟩∈id : ∀{X x y} → ⟨ x ، y ⟩ ∈ id X ⇔ x ∈ X ∧ x ≡ y
  ⟨,⟩∈id {X}{x}{y} = begin
    ⟨ x ، y ⟩ ∈ id X ⇔⟨ ⟨,⟩∈reify ⟩
    (x ∈ X ∧ y ∈ X ∧ x ≡ y)
      ⇔⟨ mk⇔ (Prod.map₂ proj₂) (λ {(x∈X , refl) → x∈X , x∈X , refl}) ⟩
    (x ∈ X ∧ x ≡ y) ∎

  ∈∘ : ∀{R S e} →
    e ∈ R ∘ S ⇔ ∃₂ λ x z → e ≡ ⟨ x ، z ⟩ ∧ ∃ λ y → ⟨ x ، y ⟩ ∈ S ∧ ⟨ y ، z ⟩ ∈ R
  ∈∘ {R}{S}{e} = begin
    e ∈ R ∘ S ⇔⟨ ∈reify ⟩
    (∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ x ∈ dom S ∧ y ∈ ran R ∧ P[ x , y ])
       ⇔⟨ ∃-cong-⇔ $ ∃-cong-⇔ $ ∧-cong-⇔ ⇔-refl $ ⇔-sym ∧-assoc ⟩
    (∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ (x ∈ dom S ∧ y ∈ ran R) ∧ P[ x , y ])
       ⇔⟨ ∃-cong-⇔ $ ∃-cong-⇔ $ ∧-cond-cong-⇔ (λ { refl → [B→A]→A∧B⇔B λ
            {(y , xy∈S , yz∈R) → to ∈dom (y , xy∈S) , to ∈ran (y , yz∈R)}}) ⟩
    (∃₂ λ x z → e ≡ ⟨ x ، z ⟩ ∧ P[ x , z ]) ∎
    where P[_,_] = λ x z → ∃ λ y → ⟨ x ، y ⟩ ∈ S ∧ ⟨ y ، z ⟩ ∈ R

  ⟨,⟩∈∘ : ∀{R S x z} → ⟨ x ، z ⟩ ∈ R ∘ S ⇔ ∃ λ y → ⟨ x ، y ⟩ ∈ S ∧ ⟨ y ، z ⟩ ∈ R
  ⟨,⟩∈∘ {R}{S}{x}{z} = begin
    ⟨ x ، z ⟩ ∈ R ∘ S ⇔⟨ ⟨,⟩∈reify ⟩
    (x ∈ dom S ∧ z ∈ ran R ∧ ∃ λ y → ⟨ x ، y ⟩ ∈ S ∧ ⟨ y ، z ⟩ ∈ R)
      ⇔⟨ ⇔-sym ∧-assoc ⟩
    ((x ∈ dom S ∧ z ∈ ran R) ∧ ∃ λ y → ⟨ x ، y ⟩ ∈ S ∧ ⟨ y ، z ⟩ ∈ R)
      ⇔⟨ [B→A]→A∧B⇔B (λ {(y , xy∈S , yz∈R) →
           to ∈dom (y , xy∈S) , to ∈ran (y , yz∈R)}) ⟩
    (∃ λ y → ⟨ x ، y ⟩ ∈ S ∧ ⟨ y ، z ⟩ ∈ R) ∎

  id-∘ : P ∶ X ⇸ Y → id Y ∘ P ≡ P
  id-∘ P-rel = antisym-⊆
    (λ x∈id∘P → case from ∈∘ x∈id∘P of λ { (u , v , refl , z , uz∈P , zv∈id) →
      case from ⟨,⟩∈id zv∈id of λ { (_ , refl) → uz∈P }})
    (λ x∈P → case from ∈× $ P-rel x∈P of λ
      { (u , u∈X , v , v∈Y , refl) →
        let vv∈id = to ⟨,⟩∈id (v∈Y , refl) in
        to ⟨,⟩∈∘ $ v , x∈P , vv∈id
        })

  ∘-id : P ∶ X ⇸ Y → P ∘ id X ≡ P
  ∘-id P-rel = antisym-⊆
    (λ x∈P∘id → case from ∈∘ x∈P∘id of λ
      { (u , v , refl , z , uz∈id , zv∈P) →
      case from ⟨,⟩∈id uz∈id of λ { (_ , refl) → zv∈P}
      })
    (λ x∈P → case from ∈× $ P-rel x∈P of λ
      { (u , u∈X , v , v∈Y , refl) →
        let uu∈id = to ⟨,⟩∈id (u∈X , refl) in
        to ⟨,⟩∈∘ $ u , uu∈id , x∈P
        })

  dom-id : dom (id X) ≡ X
  dom-id = antisym-⊆
    (λ x∈dom → case from ∈dom x∈dom of λ
      {(_ , xy∈id) → proj₁ $ from ⟨,⟩∈id xy∈id})
    λ {x} x∈X → to ∈dom (x , to ⟨,⟩∈id (x∈X , refl))
  
  ran-id : ran (id X) ≡ X
  ran-id = antisym-⊆
    (λ x∈ran → case from ∈ran x∈ran of λ
      {(_ , xy∈id) → proj₁ $ proj₂ $ from ⟨,⟩∈reify xy∈id})
    λ {x} x∈X → to ∈ran (x , to ⟨,⟩∈id (x∈X , refl))

  dom-∘ : dom (Q ∘ R) ⊆ dom R
  dom-∘ = to ∈dom F.∘
    (λ { (_ , z , xz∈R , _) → z , xz∈R})
    F.∘ Prod.map₂ (from ⟨,⟩∈∘)
    F.∘ from ∈dom

  ran-∘ : ran (Q ∘ R) ⊆ ran Q
  ran-∘ = to ∈ran F.∘
    (λ {(_ , z , _ , zy∈Q) → z , zy∈Q})
    F.∘ Prod.map₂ (from ⟨,⟩∈∘) F.∘ from ∈ran

  ∘-assoc : (P ∘ Q) ∘ R ≡ P ∘ (Q ∘ R)
  ∘-assoc {P} {Q} {R} = antisym-⊆
    (λ x∈[PQ]R → case from ∈∘ x∈[PQ]R of λ
      { (x , z , refl , y , xy∈R , yz∈PQ) →
      case from ⟨,⟩∈∘ yz∈PQ of λ
      { (w , yw∈Q , wz∈P) → to ⟨,⟩∈∘ $
      let xw∈QR = to ⟨,⟩∈∘ $ y , xy∈R , yw∈Q
      in w , xw∈QR , wz∈P}})
    (λ x∈P[QR] → case from ∈∘ x∈P[QR] of λ
      { (x , z , refl , y , xy∈QR , yz∈P) →
      case from ⟨,⟩∈∘ xy∈QR of λ
      { (w , xw∈R , wy∈Q) → to ⟨,⟩∈∘ $
      let wz∈PQ = to ⟨,⟩∈∘ $ y , wy∈Q , yz∈P
      in w , xw∈R , wz∈PQ }})

  -- empty relation

  id-∅ : id ∅ ≡ ∅
  id-∅ = antisym-⊆ (λ x∈id → case from ∈reify x∈id of λ
                      {(_ , _ , _ , x∈∅ , _) → ⊥-elim $ ∈∅ x∈∅})
                   (∅⊆ $ id ∅)

  ∘-∅ˡ : ∅ ∘ X ≡ ∅
  ∘-∅ˡ {X} = antisym-⊆ (λ x∈∅∘X → case from ∈reify x∈∅∘X of λ
                         {(_ , _ , _ , _ , _ , _ , _ , x∈∅) → ⊥-elim $ ∈∅ x∈∅})
                      (∅⊆ $ ∅ ∘ X)

  ∘-∅ʳ : X ∘ ∅ ≡ ∅
  ∘-∅ʳ {X} = antisym-⊆ (λ x∈X∘∅ → case from ∈reify x∈X∘∅ of λ
                         {(_ , _ , _ , _ , _ , _ , x∈∅ , _) → ⊥-elim $ ∈∅ x∈∅})
                      (∅⊆ $ X ∘ ∅)

  unit-rel : ｛ ⟨ x ، y ⟩ ｝ ∶ ｛ x ｝ ⇸ ｛ y ｝
  unit-rel {x}{y} z∈xy = case from ∈｛ _ ｝ z∈xy of λ {refl →
    to ⟨,⟩∈× (to ∈｛ x ｝ refl , to ∈｛ y ｝ refl)}

  ⊆-rel : R ∶ X ⇸ Y → Q ⊆ R → Q ∶ X ⇸ Y
  ⊆-rel = flip trans-⊆

  ⋃-rel : A ⊆ Rel X Y → ⋃ A ∶ X ⇸ Y
  ⋃-rel {A}{X}{Y} A⊆Rel {p} p∈⋃A =
    case from ∈⋃ p∈⋃A of λ { (R , R∈A , p∈R) →
    (from ∈Rel (A⊆Rel R∈A) ∶ R ⊆ X × Y) p∈R}

-- properties of relations
  
injective : (R : set) → Set
injective R = ∀{x x' y} → ⟨ x ، y ⟩ ∈ R → ⟨ x' ، y ⟩ ∈ R → x ≡ x'

surjective : (R Y : set) → Set
surjective R Y = ⋀ y ∈ Y , ∃ λ x → ⟨ x ، y ⟩ ∈ R

rel-surj : .(R ∶ X ⇸ Y) → Set
rel-surj {R}{X}{Y} _ = surjective R Y

abstract
  surj-ran : (r-rel : R ∶ X ⇸ Y) → rel-surj r-rel ⇔ ran R ≡ Y
  surj-ran r-rel = mk⇔
    (λ ∀y∃x → antisym-⊆
      ((λ { (x , xy∈R) → proj₂ $ from ⟨,⟩∈× $ r-rel xy∈R})
        F.∘ from ∈ran)
      λ y∈Y → to ∈ran $ ∀y∃x y∈Y)
    λ { refl x∈ran → from ∈ran x∈ran}

  triv-surj : surjective R (ran R)
  triv-surj = from ∈ran

  -- relational images
  
  infixr 22 _⃗[_] _⃖[_]
  _⃗[_] : (R X : set) → set
  R ⃗[ X ] = ｛  y ∈ ran R ∣ (⋁ x ∈ X , ⟨ x ، y ⟩ ∈ R) ｝

  ∈⃗[] : y ∈ R ⃗[ X ] ⇔ ⋁ x ∈ X , ⟨ x ، y ⟩ ∈ R
  ∈⃗[] {y}{R}{X} = begin
    y ∈ R ⃗[ X ] ⇔⟨ ∈｛ y ∈ ran R ∣ (⋁ x ∈ X , ⟨ x ، y ⟩ ∈ R) ｝ ⟩
    (y ∈ ran R ∧ (⋁ x ∈ X , ⟨ x ، y ⟩ ∈ R))
      ⇔⟨ [B→A]→A∧B⇔B (λ { (x , _ , xy∈R) → to ∈ran $ x , xy∈R}) ⟩
    (⋁ x ∈ X , ⟨ x ، y ⟩ ∈ R) ∎
  
  _⃖[_] : R ∶ X ⇸ Y → (Y' : set) → set
  _⃖[_] {R}{X} r-rel Y' = ｛  x ∈ X ∣ (∀{y} → ⟨ x ، y ⟩ ∈ R → y ∈ Y') ｝

  open import ZF.Foundation.Axiom.Nonconstructive

  ∈⃖[] : (r-rel : R ∶ X ⇸ Y)
    → -----------------------------------------------------
    ∀{x} → x ∈ r-rel ⃖[ Y' ] ⇔ x ∈ X ∧ ¬ ∃ λ y → ⟨ x ، y ⟩ ∈ R ∧ y ∉ Y'
  ∈⃖[] {R}{X}{Y}{Y'} r-rel {x} = begin
    x ∈ r-rel ⃖[ Y' ] ⇔⟨ ∈｛  x ∈ X ∣ (∀{y} → ⟨ x ، y ⟩ ∈ R → y ∈ Y') ｝ ⟩
    (x ∈ X ∧ ∀{y} → ⟨ x ، y ⟩ ∈ R → y ∈ Y')
      ⇔⟨ mk⇔ (λ { (x∈X , xy∈R→y∈Y') → x∈X , λ
                { (y , xy∈R , y∉Y') → y∉Y' $ xy∈R→y∈Y' xy∈R}})
             (λ {(x∈X , ¬∃xy∈R∧y∉Y') →
                x∈X , λ {y} xy∈R → by-contradiction λ y∉Y' →
                      ¬∃xy∈R∧y∉Y' $ y , xy∈R , y∉Y'}) ⟩
    (x ∈ X ∧ ¬ ∃ λ y → ⟨ x ، y ⟩ ∈ R ∧ y ∉ Y') ∎

  -- inverse

  infixl 30 _⁻¹
  _⁻¹ : (R : set) → set
  R ⁻¹ = ｛ e ∈ ran R × dom R ∣ (∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ ⟨ y ، x ⟩ ∈ R) ｝

  ∈⁻¹ : ∀{R e} →
    e ∈ R ⁻¹ ⇔
    ∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ ⟨ y ، x ⟩ ∈ R
  ∈⁻¹ {R}{e} = begin
    e ∈ R ⁻¹ ⇔⟨ ∈｛ e ∈ ran R × dom R ∣ ϕ e ｝ ⟩
    (e ∈ ran R × dom R ∧ ∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ ⟨ y ، x ⟩ ∈ R)
      ⇔⟨ [B→A]→A∧B⇔B (λ { (x , y , refl , yx∈R) →
        to ⟨,⟩∈× $ to ∈ran (y , yx∈R) , to ∈dom (x , yx∈R)}) ⟩
    (∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ ⟨ y ، x ⟩ ∈ R) ∎
    where ϕ = λ e → (∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ ⟨ y ، x ⟩ ∈ R)

  ⟨,⟩∈⁻¹ : ∀{R x y} → ⟨ x ، y ⟩ ∈ R ⁻¹ ⇔ ⟨ y ، x ⟩ ∈ R
  ⟨,⟩∈⁻¹ {R}{x}{y} = begin
    ⟨ x ، y ⟩ ∈ R ⁻¹ ⇔⟨ ∈｛ e ∈ ran R × dom R ∣ ϕ e ｝ ⟩
    (⟨ x ، y ⟩ ∈ ran R × dom R ∧ ϕ ⟨ x ، y ⟩)
      ⇔⟨ mk⇔ (λ { (_ , x' , y' , xy≡x'y' , x'y'∈R) →
                case from ⟨,⟩≡ xy≡x'y' of λ { (refl , refl) → x'y'∈R}})
             (λ yx∈R → to ⟨,⟩∈× (to ∈ran (y , yx∈R) , to ∈dom (x , yx∈R)) ,
                       x , y , refl , yx∈R) ⟩
    ⟨ y ، x ⟩ ∈ R ∎
    where ϕ = λ e → (∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ ⟨ y ، x ⟩ ∈ R)

  dom⁻¹ : dom (R ⁻¹) ≡ ran R
  dom⁻¹ = antisym-⊆
    (λ x∈dom⁻¹ → case from ∈dom x∈dom⁻¹ of λ
      { (y , xy∈R⁻¹) → to ∈ran $ y , from ⟨,⟩∈⁻¹ xy∈R⁻¹})
    (λ x∈ran → case from ∈ran x∈ran of λ
      { (y , yx∈R) → to ∈dom $ y , to ⟨,⟩∈⁻¹ yx∈R})

  ran⁻¹ : ran (R ⁻¹) ≡ dom R
  ran⁻¹ = antisym-⊆
    (λ x∈ran⁻¹ → case from ∈ran x∈ran⁻¹ of λ
      { (y , xy∈R⁻¹) → to ∈dom $ y , from ⟨,⟩∈⁻¹ xy∈R⁻¹})
    (λ x∈dom → case from ∈dom x∈dom of λ
      { (y , yx∈R) → to ∈ran $ y , to ⟨,⟩∈⁻¹ yx∈R})

  ⁻¹-rel : R ∶ X ⇸ Y → R ⁻¹ ∶ Y ⇸ X
  ⁻¹-rel {R} R-rel e∈R⁻¹ = case from ∈⁻¹ e∈R⁻¹ of λ
    { (x , y , refl , yx∈R) → to ⟨,⟩∈× $
      ran⊆ R-rel (to ∈ran $ y , yx∈R) ,
      dom⊆ R-rel (to ∈dom $ x , yx∈R)}

  ⁻¹-involutive : R ∶ X ⇸ Y → R ⁻¹ ⁻¹ ≡ R
  ⁻¹-involutive {R} R-rel = rel-ext (⁻¹-rel $ ⁻¹-rel R-rel) R-rel λ {x}{y} →
    mk⇔ (from ⟨,⟩∈⁻¹ F.∘ from ⟨,⟩∈⁻¹)
        (to ⟨,⟩∈⁻¹ F.∘ to ⟨,⟩∈⁻¹)

  id⁻¹ : id X ⁻¹ ≡ id X
  id⁻¹ = rel-ext (⁻¹-rel id-rel) id-rel $ mk⇔
    (λ xy∈id⁻¹ → case from ⟨,⟩∈id $ from ⟨,⟩∈⁻¹ xy∈id⁻¹ of λ {(x∈X , refl) →
      to ⟨,⟩∈id (x∈X , refl)})
    (λ xy∈id → case from ⟨,⟩∈id xy∈id of λ { (_ , refl) → to ⟨,⟩∈⁻¹ xy∈id })

  ∘⁻¹ : (R ∘ S) ⁻¹ ≡ S ⁻¹ ∘ R ⁻¹
  ∘⁻¹ = antisym-⊆
    (λ x∈RS⁻¹ → case from ∈⁻¹ x∈RS⁻¹ of λ
      { (x , y , refl , yx∈RS) → case from ⟨,⟩∈∘ yx∈RS of λ
      { (z , yz∈S , zx∈R) → to ⟨,⟩∈∘ $
        z , to ⟨,⟩∈⁻¹ zx∈R , to ⟨,⟩∈⁻¹ yz∈S}})
    (λ x∈S⁻¹R⁻¹ → case from ∈∘ x∈S⁻¹R⁻¹ of λ
      { (x , y , refl , z , xz∈R⁻¹ , zy∈S⁻¹) → to ⟨,⟩∈⁻¹ $ to ⟨,⟩∈∘ $
        z , from ⟨,⟩∈⁻¹ zy∈S⁻¹ , from ⟨,⟩∈⁻¹ xz∈R⁻¹})

  ⁻¹-⊆ : R ∶ X ⇸ Y → R ⁻¹ ⊆ S ⁻¹ ⇔ R ⊆ S
  ⁻¹-⊆ {R}{X}{Y}{S} R-rel = mk⇔
    (λ R⁻¹⊆S⁻¹ {p} p∈R → case from ∈× $ R-rel p∈R of λ
      { (_ , _ , _ , _ , refl) → from ⟨,⟩∈⁻¹ $ R⁻¹⊆S⁻¹ $ to ⟨,⟩∈⁻¹ p∈R})
    λ R⊆S {p} p∈R⁻¹ → case from ∈× $ ⁻¹-rel R-rel p∈R⁻¹ of λ
      { (_ , _ , _ , _ , refl) → to ⟨,⟩∈⁻¹ $ R⊆S $ from ⟨,⟩∈⁻¹ p∈R⁻¹}

  open import Function.Reasoning

  id⊆R∘R⁻¹⇔surj : id Y ⊆ R ∘ R ⁻¹ ⇔ surjective R Y
  id⊆R∘R⁻¹⇔surj {Y}{R} = mk⇔
    (λ id⊆R∘R⁻¹ {y} y∈Y →
         to ⟨,⟩∈id (y∈Y , refl) ∶ ⟨ y ، y ⟩ ∈ id Y
      |> id⊆R∘R⁻¹ {_} ∶ ⟨ y ، y ⟩ ∈ R ∘ R ⁻¹
      |> Prod.map₂ proj₂ F.∘ from ⟨,⟩∈∘ ∶ ∃ λ x → ⟨ x ، y ⟩ ∈ R)
    λ ∀y∃xRy {p} p∈id → case from ∈id p∈id of λ
      { (y , refl , y∈Y) →
           ∀y∃xRy y∈Y ∶ (∃ λ x → ⟨ x ، y ⟩ ∈ R)
        |> Prod.map₂ (λ xy∈R → to ⟨,⟩∈⁻¹ xy∈R , xy∈R)
          ∶ (∃ λ x → ⟨ y ، x ⟩ ∈ R ⁻¹ ∧ ⟨ x ، y ⟩ ∈ R)
        |> to (⟨,⟩∈∘ {R}{R ⁻¹}) ∶ ⟨ y ، y ⟩ ∈ R ∘ R ⁻¹
        }

  R⁻¹∘R⊆id⇔inj : R ⁻¹ ∘ R ⊆ id (dom R) ⇔ injective R
  R⁻¹∘R⊆id⇔inj {R} = begin
    R ⁻¹ ∘ R ⊆ id (dom R) ≡⟨⟩
    (∀{p} → p ∈ R ⁻¹ ∘ R → p ∈ id (dom R))
      ⇔⟨ ∀-reform ⟨_،_⟩ (λ e∈R⁻¹∘R → case from ∈∘ e∈R⁻¹∘R of λ
           { (x , z , refl , _) → x , z , refl}) ⟩
    (∀{x x'} → ⟨ x ، x' ⟩ ∈ R ⁻¹ ∘ R → ⟨ x ، x' ⟩ ∈ id (dom R))
      ⇔⟨ ∀′-cong-⇔ (λ {x} → ∀′-cong-⇔ λ {x'} → begin
      (⟨ x ، x' ⟩ ∈ R ⁻¹ ∘ R → ⟨ x ، x' ⟩ ∈ id (dom R))
        ⇔⟨ →-cong-⇔ ⟨,⟩∈∘ ⟨,⟩∈id ⟩
      ((∃ λ y → ⟨ x ، y ⟩ ∈ R ∧ ⟨ y ، x' ⟩ ∈ R ⁻¹) → (x ∈ dom R ∧ x ≡ x'))
        ⇔⟨ ∃[]→⇔∀[→] ⟩
      (∀{y}→ ⟨ x ، y ⟩ ∈ R ∧ ⟨ y ، x' ⟩ ∈ R ⁻¹ → x ∈ dom R ∧ x ≡ x')
        ⇔⟨ ∀′-cong-⇔ (λ {y} → begin
          (⟨ x ، y ⟩ ∈ R ∧ ⟨ y ، x' ⟩ ∈ R ⁻¹ → x ∈ dom R ∧ x ≡ x')
            ⇔⟨ →-cong-⇔ (∧-cong-⇔ ⇔-refl ⟨,⟩∈⁻¹) ⇔-refl ⟩
          (⟨ x ، y ⟩ ∈ R ∧ ⟨ x' ، y ⟩ ∈ R → x ∈ dom R ∧ x ≡ x')
            ⇔⟨ ∧→⇔→→ ⟩
          (⟨ x ، y ⟩ ∈ R → ⟨ x' ، y ⟩ ∈ R → x ∈ dom R ∧ x ≡ x')
            ⇔⟨ →-cond-cong-⇔ (λ xy∈R →
               →-cong-⇔ ⇔-refl $ mk⇔ proj₂ $ to ∈dom (_ , xy∈R) ,_) ⟩
          (⟨ x ، y ⟩ ∈ R → ⟨ x' ، y ⟩ ∈ R → x ≡ x') ∎ )⟩
      (∀{y} → ⟨ x ، y ⟩ ∈ R → ⟨ x' ، y ⟩ ∈ R → x ≡ x') ∎ )⟩
    (∀{x x' y} → ⟨ x ، y ⟩ ∈ R → ⟨ x' ، y ⟩ ∈ R → x ≡ x') ≡⟨⟩
    injective R ∎

  -- domain and codomain restriction
  
  restrict : (R X Y : set) → set
  restrict R X Y = ｛ e ∈ R ∣ (∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ x ∈ X ∧ y ∈ Y) ｝
  
  ∈restrict :
    e ∈ restrict R X Y ⇔
    e ∈ R ∧ ∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ x ∈ X ∧ y ∈ Y
  ∈restrict {_}{R}{X}{Y} =
    ∈｛ e ∈ R ∣ (∃₂ λ x y → e ≡ ⟨ x ، y ⟩ ∧ x ∈ X ∧ y ∈ Y) ｝

  ⟨,⟩∈restrict : ⟨ x ، y ⟩ ∈ restrict R X Y ⇔ ⟨ x ، y ⟩ ∈ R ∧ x ∈ X ∧ y ∈ Y
  ⟨,⟩∈restrict {x}{y}{R}{X}{Y} = begin
    ⟨ x ، y ⟩ ∈ restrict R X Y ⇔⟨ ∈restrict ⟩
    (⟨ x ، y ⟩ ∈ R ∧ ∃₂ λ x' y' → ⟨ x ، y ⟩ ≡ ⟨ x' ، y' ⟩ ∧ x' ∈ X ∧ y' ∈ Y)
      ⇔⟨ ∧-cong-⇔ ⇔-refl $ mk⇔
           (λ { (x' , y' , xy≡x'y' , x'∈X , y'∈Y) →
              case from ⟨,⟩≡ xy≡x'y' of λ
              { (refl , refl) → x'∈X , y'∈Y}})
           (λ { (x∈X , y∈Y) → x , y , refl , x∈X , y∈Y}) ⟩
    (⟨ x ، y ⟩ ∈ R ∧ x ∈ X ∧ y ∈ Y) ∎

  restrict-rel : R ∶ X ⇸ Y → restrict R X' Y' ∶ X ∩ X' ⇸ Y ∩ Y'
  restrict-rel R-rel =
    (λ { (xy∈R , x , y , refl , x∈X' , y∈Y') → to ⟨,⟩∈× $
      to ∈∩ (dom⊆ R-rel (to ∈dom $ y , xy∈R) , x∈X') ,
      to ∈∩ (ran⊆ R-rel (to ∈ran $ x , xy∈R) , y∈Y')})
    F.∘ from ∈restrict 

  restrict≡restrict-∩ :
    restrict R X Y ≡ restrict R (X ∩ dom R)(Y ∩ ran R)
  restrict≡restrict-∩ = antisym-⊆
    (λ x∈r∩ → case from ∈restrict x∈r∩ of λ
      { (xy∈R , x , y , refl , x∈X , y∈Y) → to ⟨,⟩∈restrict $
        xy∈R , to ∈∩ (x∈X , to ∈dom (y , xy∈R)) ,
               to ∈∩ (y∈Y , to ∈ran (x , xy∈R))})
    (λ x∈r → case from ∈restrict x∈r of λ
      { (xy∈R , x , y , refl , x∈X∩ , y∈Y∩) → to ⟨,⟩∈restrict $
        xy∈R , A∩B⊆A x∈X∩ , A∩B⊆A y∈Y∩ } )

  restrict⁻¹ : restrict R X Y ⁻¹ ≡ restrict (R ⁻¹) Y X
  restrict⁻¹ = antisym-⊆
    (λ e∈r⁻¹ → case from ∈⁻¹ e∈r⁻¹ of λ
      { (y , x , refl , xy∈r) → case from ⟨,⟩∈restrict xy∈r of λ
      { (yx∈R , x∈X , y∈Y) → to ⟨,⟩∈restrict $
        to ⟨,⟩∈⁻¹ yx∈R , y∈Y , x∈X} })
    (λ e∈r[⁻¹] → case from ∈restrict e∈r[⁻¹] of λ
      { (yx∈R⁻¹ , y , x , refl , y∈Y , x∈X) → to ⟨,⟩∈⁻¹ $ to ⟨,⟩∈restrict $
        from ⟨,⟩∈⁻¹ yx∈R⁻¹ , x∈X , y∈Y})

  dom-restrict⊆ : dom (restrict R X Y) ⊆ dom R ∩ X
  dom-restrict⊆ {R}{X}{Y}{x} x∈dom-r = case from ∈dom x∈dom-r of λ
    { (y , xy∈rR) → case from ⟨,⟩∈restrict xy∈rR of λ
    { (xy∈R , x∈X , y∈Y) → to ∈∩ $ to ∈dom (y , xy∈R) , x∈X}}

  dom-restrict≡ : dom (restrict R X (ran R)) ≡ dom R ∩ X
  dom-restrict≡ {R}{X} = antisym-⊆ dom-restrict⊆
    (λ { {x} x∈∩ → case from ∈∩ x∈∩ of λ { (x∈dom , x∈X) →
      case from ∈dom x∈dom of λ { (y , xy∈R) →
      to ∈dom $ y , to ⟨,⟩∈restrict (xy∈R , x∈X , to ∈ran (x , xy∈R))}}})

  open import ZF.Reasoning as ≡
    using (begin⊆_; step-⊆) renaming (_∎ to _■)

  ran-restrict⊆ : ran (restrict R X Y) ⊆ ran R ∩ Y
  ran-restrict⊆ {R}{X}{Y} = begin⊆
    ran (restrict R X Y) ≡.≡⟨ sym dom⁻¹ ⟩
    dom (restrict R X Y ⁻¹) ≡.≡⟨ cong dom restrict⁻¹ ⟩
    dom (restrict (R ⁻¹) Y X) ⊆⟨ dom-restrict⊆ ⟩
    dom (R ⁻¹) ∩ Y ≡.≡⟨ cong (_∩ Y) dom⁻¹ ⟩
    ran R ∩ Y ■

  ran-restrict≡ : ran (restrict R (dom R) Y) ≡ ran R ∩ Y
  ran-restrict≡ {R}{Y} = ≡.begin
    ran (restrict R (dom R) Y) ≡.≡⟨ sym dom⁻¹ ⟩
    dom (restrict R (dom R) Y ⁻¹) ≡.≡⟨ cong dom restrict⁻¹ ⟩
    dom (restrict (R ⁻¹) Y (dom R))
      ≡.≡⟨ cong (λ P → dom (restrict (R ⁻¹) Y P)) $ sym ran⁻¹ ⟩
    dom (restrict (R ⁻¹) Y (ran (R ⁻¹))) ≡.≡⟨ dom-restrict≡ ⟩
    dom (R ⁻¹) ∩ Y ≡.≡⟨ cong (_∩ Y) dom⁻¹ ⟩
    ran R ∩ Y ≡.∎

  restrict⊆ : restrict R X Y ⊆ R
  restrict⊆ = sep⊆ _ _

  restrict-cong-⊆ : R ⊆ P → X ⊆ X' → Y ⊆ Y' → restrict R X Y ⊆ restrict P X' Y'
  restrict-cong-⊆ {R}{P}{X}{X'}{Y}{Y'} R⊆P X⊆X' Y⊆Y' =
    (λ { (xy∈R , x , y , refl , x∈X , y∈Y) → to ⟨,⟩∈restrict $
      R⊆P xy∈R , X⊆X' x∈X , Y⊆Y' y∈Y})
    F.∘ from ∈restrict

  restrict-transparent : R ∶ X ⇸ Y → restrict R (dom R)(ran R) ≡ R
  restrict-transparent R-rel = rel-ext (restrict-rel R-rel) R-rel $ mk⇔ restrict⊆
    λ xy∈R → to ⟨,⟩∈restrict (xy∈R , to ∈dom (_ , xy∈R) , to ∈ran (_ , xy∈R))

  restrict-transparent-⊆ : R ∶ X' ⇸ Y' → dom R ⊆ X → ran R ⊆ Y → restrict R X Y ≡ R
  restrict-transparent-⊆ {R}{_}{_}{X}{Y} R-rel dom⊆X ran⊆Y = ≡.begin
    restrict R X Y ≡.≡⟨ restrict≡restrict-∩ ⟩
    restrict R (X ∩ dom R)(Y ∩ ran R)
      ≡.≡⟨ cong₂ (restrict R) (A⊆B→B∩A≡A dom⊆X) (A⊆B→B∩A≡A ran⊆Y) ⟩
    restrict R (dom R)(ran R) ≡.≡⟨ restrict-transparent R-rel ⟩
    R ■

  restrict-∘ : ∀{f g} →
    g ∘ f ≡ restrict g (ran f)(ran g) ∘ restrict f (dom f)(dom g)
  restrict-∘ {f} {g} = rel-ext reify-rel reify-rel $ λ {x}{y} → mk⇔
    (λ xy∈gf → case from ⟨,⟩∈∘ xy∈gf of λ { (z , xz∈f , zy∈g) →
      to ⟨,⟩∈∘ $ z ,
      to ⟨,⟩∈restrict (xz∈f , to ∈dom (z , xz∈f) , to ∈dom (y , zy∈g)) ,
      to ⟨,⟩∈restrict (zy∈g , to ∈ran (x , xz∈f) , to ∈ran (z , zy∈g))})
    λ xy∈rgrf → case from ⟨,⟩∈∘ xy∈rgrf of λ { (z , xz∈rf , zy∈rg) →
      to ⟨,⟩∈∘ $ z , restrict⊆ xz∈rf , restrict⊆ zy∈rg}
    
