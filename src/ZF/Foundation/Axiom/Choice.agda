{-# OPTIONS --exact-split #-}
module ZF.Foundation.Axiom.Choice where

open import ZF.Foundation.Axiom.Choice.Base public
open import ZF.Foundation.Axiom.Choice.Properties public
