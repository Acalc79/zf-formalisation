{-# OPTIONS --exact-split #-}
module ZF.Function where

open import ZF.Foundation as F
  hiding (id; _[_]'; _[_]≡_; _[_∈dom]) renaming (_∘_ to _o_)
open import ZF.Foundation.Function.Base as Base
  hiding (_[_]'; _[_]≡_; _[_∈dom]) public

_∶_⇀_ : (R X Y : set) → Set
R ∶ X ⇀ Y =
  (∀{x y y'} → ⟨ x ، y ⟩ ∈ R → ⟨ x ، y' ⟩ ∈ R → y ≡ y') ∧
  R ∶ X ⇸ Y

_∶_⟶_ : (R X Y : set) → Set
R ∶ X ⟶ Y = dom R ≡ X ∧ R ∶ X ⇀ Y

PFun Fun _⇒_ : (X Y : set) → set
PFun X Y = ｛ f ∈ Rel X Y ∣ f ∶ X ⇀ Y ｝
X ⇒ Y = ｛ f ∈ Rel X Y ∣ f ∶ X ⟶ Y ｝
Fun = _⇒_

private variable X Y Z X' Y' Z' R f g h x y z : set

abstract
  ∈PFun : f ∈ PFun X Y ⇔ f ∶ X ⇀ Y
  ∈PFun {X = X}{Y} = mk⇔
    (proj₂ o from ∈pf)
    (λ { fpart@(_ , f-rel) → to ∈pf $ to ∈Rel f-rel , fpart})
    where ∈pf = ∈｛ f ∈ Rel X Y ∣ f ∶ X ⇀ Y ｝

  ∈Fun : f ∈ Fun X Y ⇔ f ∶ X ⟶ Y
  ∈Fun {X = X}{Y} = mk⇔
    (proj₂ o from ∈fun)
    (λ { ffun@(_ , _ , f-rel) → to ∈fun $ to ∈Rel f-rel , ffun})
    where ∈fun = ∈｛ f ∈ Rel X Y ∣ f ∶ X ⟶ Y ｝

  ∶⇀-cong-⊆ : X' ⊆ X → Y' ⊆ Y → f ∶ X' ⇀ Y' → f ∶ X ⇀ Y
  ∶⇀-cong-⊆ X'⊆X Y'⊆Y (well-def , f-rel) = well-def , ∶⇸-cong-⊆ X'⊆X Y'⊆Y f-rel

  ∶⟶-cong-⊆ : X' ≡ X → Y' ⊆ Y → f ∶ X' ⟶ Y' → f ∶ X ⟶ Y
  ∶⟶-cong-⊆ refl Y'⊆Y (refl , fpartial) = refl , ∶⇀-cong-⊆ refl-⊆ Y'⊆Y fpartial

pfun→rel : f ∶ X ⇀ Y → f ∶ X ⇸ Y
pfun→rel = proj₂

fun→pfun : f ∶ X ⟶ Y → f ∶ X ⇀ Y
fun→pfun = proj₂

fun→rel : f ∶ X ⟶ Y → f ∶ X ⇸ Y
fun→rel = pfun→rel F.∘ fun→pfun

open import ZF.Relation.Base as RelBase hiding (restrict; tighten)
open RelBase public
  using ( id; _∘_; restrict⊆
        ; injective; surjective; triv-surj
        ; _⃗[_]; _⃖[_]; _⁻¹)
open import Function.Reasoning
open import ZF.Reasoning

--- Properties of partial functions

abstract
  ∶⇀→⁻¹-inj : f ∶ X ⇀ Y → injective (f ⁻¹)
  ∶⇀→⁻¹-inj (f-pfun , f-rel) xy∈f⁻¹ x'y∈f⁻¹ =
      f-pfun (from ⟨,⟩∈⁻¹ xy∈f⁻¹)(from ⟨,⟩∈⁻¹ x'y∈f⁻¹)

  rel-⁻¹inj→∶⇀ : f ∶ X ⇸ Y → injective (f ⁻¹) → f ∶ X ⇀ Y
  rel-⁻¹inj→∶⇀ f-rel inj-f⁻¹ =
    (λ xy∈f xy'∈f → inj-f⁻¹ (to ⟨,⟩∈⁻¹ xy∈f)(to ⟨,⟩∈⁻¹ xy'∈f)) ,
    f-rel

  ∶⇀→is-set-fun : f ∶ X ⇀ Y → is-set-fun f
  ∶⇀→is-set-fun (f-fun , f-rel) =
    (λ x∈f → case from ∈× $ f-rel x∈f of λ
      { (x , _ , y , _ , refl) → x , y , refl} ) ,
    f-fun

  id-is-pfun : id X ∶ X ⇀ X
  id-is-pfun =
    (λ {x}{y}{y'} xy∈R xy'∈R →
    begin y ≡⟨ sym $ proj₂ $ proj₂ $ from ⟨,⟩∈reify xy∈R ⟩
          x ≡⟨ proj₂ $ proj₂ $ from ⟨,⟩∈reify xy'∈R ⟩
          y' ∎) ,
    id-rel
  
  ∘-pfun : g ∶ Y ⇀ Z → f ∶ X ⇀ Y → g ∘ f ∶ X ⇀ Z
  ∘-pfun (g-func , g-rel) (f-func , f-rel) =  
    (λ {x}{y}{y'} xy∈gf xy'∈gf →
    case from ⟨,⟩∈reify xy∈gf , from ⟨,⟩∈reify xy'∈gf of λ
    { ((_ , _ , z , xz∈f , zy∈g) , (_ , _ , z' , xz'∈f , z'y'∈g)) →
       f-func xz∈f xz'∈f ∶ z ≡ z'
    |> (λ {refl → g-func zy∈g z'y'∈g}) ∶ y ≡ y'}) ,
    ∘-rel g-rel f-rel

  ∅-pfun : ∅ ∶ X ⇀ Y
  ∅-pfun = ⊥-elim o ∈∅ , ∅-rel

  unit-pfun : ｛ ⟨ x ، y ⟩ ｝ ∶ ｛ x ｝ ⇀ ｛ y ｝
  unit-pfun {x}{y} =
    (λ {x₀}{y₀}{y₁} xy₀∈xy xy₁∈xy → begin
      y₀ ≡⟨ proj₂ $ from ⟨,⟩≡ $ from ∈｛ _ ｝ xy₀∈xy ⟩
      y ≡⟨ sym $ proj₂ $ from ⟨,⟩≡ $ from ∈｛ _ ｝ xy₁∈xy ⟩
      y₁ ∎) ,
    unit-rel

  const-pfun : X × ｛ y ｝ ∶ X ⇀ ｛ y ｝
  const-pfun {X}{y} = (λ xy∈Xy xy'∈Xy →
    case proj₂ (from ⟨,⟩∈× xy∈Xy) , proj₂ (from ⟨,⟩∈× xy'∈Xy) of λ
      { (y∈y , y'∈y) → case from ∈｛ y ｝ y∈y , from ∈｛ y ｝ y'∈y of λ
      { (refl , refl) → refl }}) ,
    ×-rel

  ⊆-pfun : ∀{X Y f g} → f ∶ X ⇀ Y → g ⊆ f → g ∶ X ⇀ Y
  ⊆-pfun (!f , f-rel) g⊆f =
    (λ xy∈g xy'∈g → !f (g⊆f xy∈g)(g⊆f xy'∈g)) , ⊆-rel f-rel g⊆f

  ∪-pfun : disjoint X X' → g ∶ X ⇀ Y → f ∶ X' ⇀ Y' → g ∪ f ∶ X ∪ X' ⇀ Y ∪ Y'
  ∪-pfun disj (!g , g-rel) (!f , f-rel) =
    (λ {x}{y}{y'} xy∈gf xy'∈gf → case from ∈∪ xy∈gf , from ∈∪ xy'∈gf of λ
      { (inj₁ xy∈g , inj₁ xy'∈g) → !g xy∈g xy'∈g
      ; (inj₂ xy∈f , inj₂ xy'∈f) → !f xy∈f xy'∈f
      ; (inj₁ xy∈g , inj₂ xy'∈f) → ⊥-elim $ disj x $
        proj₁ (from ⟨,⟩∈× $ g-rel xy∈g) ,
        proj₁ (from ⟨,⟩∈× $ f-rel xy'∈f)
      ; (inj₂ xy∈f , inj₁ xy'∈g) → ⊥-elim $ disj x $
        proj₁ (from ⟨,⟩∈× $ g-rel xy'∈g) ,
        proj₁ (from ⟨,⟩∈× $ f-rel xy∈f)
      } ) ,
    ∪Rel g-rel f-rel

  --- Properties of functions
  
  ∶⟶→is-set-fun : f ∶ X ⟶ Y → is-set-fun f
  ∶⟶→is-set-fun = ∶⇀→is-set-fun o fun→pfun

  is-set-fun→∶⟶ : is-set-fun f → f ∶ dom f ⟶ ran f
  is-set-fun→∶⟶ (f-rel , !f) =
    refl , !f ,
    RelBase.tighten (λ e∈f → to ∈× $ case f-rel e∈f of λ
      { (x , y , refl) → x , to ∈dom (y , e∈f) , y , to ∈ran (x , e∈f) , refl})

  ⇀-tighten : f ∶ X ⇀ Y → f ∶ dom f ⟶ ran f
  ⇀-tighten = is-set-fun→∶⟶ o ∶⇀→is-set-fun

  ⟶-tighten : f ∶ X ⟶ Y → f ∶ X ⟶ ran f
  ⟶-tighten (refl , f-pfun) = ⇀-tighten f-pfun

  _[_∈dom] : f ∶ X ⟶ Y → ⋀ x ∈ X , ⟨ x ، f [ x ] ⟩ ∈ f
  f@(refl , _) [ x∈X ∈dom] = ∶⟶→is-set-fun f Base.[ x∈X ∈dom]
  
  val∈ran : f ∶ X ⟶ Y → ⋀ x ∈ X , f [ x ] ∈ Y
  val∈ran f@(_ , _ , f-rel) x∈X =
    proj₂ $ from ⟨,⟩∈× $ f-rel $ f [ x∈X ∈dom]

  ∶⟶→⁻¹-surj : f ∶ X ⟶ Y → surjective (f ⁻¹) X
  ∶⟶→⁻¹-surj f-fun@(refl , _ , f-rel) =
    to (surj-ran $ ⁻¹-rel f-rel) ran⁻¹

  ∶⇀-⁻¹surj→∶⟶ : f ∶ X ⇀ Y → surjective (f ⁻¹) X → f ∶ X ⟶ Y
  ∶⇀-⁻¹surj→∶⟶ {f}{X}{Y} f-pfun@(_ , f-rel) f⁻¹surj =
      (begin dom f ≡⟨ sym ran⁻¹ ⟩
             ran (f ⁻¹) ≡⟨ from (surj-ran $ ⁻¹-rel f-rel) f⁻¹surj ⟩
             X ∎) , f-pfun

  _[_]≡ : f ∶ X ⟶ Y → (x : set) → (x ∈ X ∧ f [ x ] ≡ y) ⇔ ⟨ x ، y ⟩ ∈ f
  f@(refl , _) [ x ]≡ = mk⇔
    (λ { (x∈X , refl) → f [ x∈X ∈dom]})
    (λ { xy∈f → to (∶⟶→is-set-fun f Base.[ x ]≡ _) xy∈f})

  fun-ext : ∀{f'}(funf : f ∶ X ⟶ Y)(funf' : f' ∶ X ⟶ Y) →
    (⋀ x ∈ X , f [ x ] ≡ f' [ x ])
    → ----------------------------------------
    f ≡ f'
  fun-ext {f}{f' = f'}
    funf@(refl , _ , f-rel)
    funf'@(dom'≡dom , _ , f'-rel) ∀x,fx≡f'x = antisym-⊆
    (λ x∈f → case from ∈× $ f-rel x∈f of λ
      { (u , u∈domf , v , v∈Y , refl) →
      case proj₂ $ to (funf [ u ]≡) x∈f of λ
      { refl → funf' [ u∈domf  ∈dom] ∶ ⟨ u ، f' [ u ] ⟩ ∈ f'
            |> subst (λ v → ⟨ u ، v ⟩ ∈ f') (sym $ ∀x,fx≡f'x u∈domf)}})
    (λ x∈f' → case from ∈× $ f'-rel x∈f' of λ
      { (u , u∈domf' , v , v∈Y , refl) →
      case proj₂ $ to (funf' [ u ]≡) x∈f' of λ
      { refl → funf [ u∈domf' ∈dom] ∶ ⟨ u ، f [ u ] ⟩ ∈ f
            |> subst (λ v → ⟨ u ، v ⟩ ∈ f) (∀x,fx≡f'x u∈domf')}})

-- injectivity for functions

fun-injective : (f X : set) → Set
fun-injective f X = ⋀ x ∈ X , ⋀ x' ∈ X , (f [ x ] ≡ f [ x' ] → x ≡ x')

abstract
  fun-inj⇔inj : f ∶ X ⟶ Y → fun-injective f X ⇔ injective f
  fun-inj⇔inj {f} f-fun@(refl , _) = mk⇔
    (λ fun-inj {x}{x'}{y} xy∈f x'y∈f →
      fun-inj (to ∈dom $ y , xy∈f)(to ∈dom $ y , x'y∈f) $ begin
      f [ x ] ≡⟨ proj₂ $ to (f-fun [ x ]≡) xy∈f ⟩
      y ≡⟨ sym $ proj₂ $ to (f-fun [ x' ]≡) x'y∈f ⟩
      f [ x' ] ∎)
    (λ inj {x} x∈X {x'} x'∈X fx≡fx' →
      inj (subst (λ y → ⟨ x ، y ⟩ ∈ f) fx≡fx' $ f-fun [ x∈X ∈dom])
          (f-fun [ x'∈X ∈dom]))

  id-fun : id X ∶ X ⟶ X
  id-fun =
    antisym-⊆
      (λ x∈dom → case from ∈dom x∈dom of λ
        { (_ , xx'∈id) → proj₁ $ from ⟨,⟩∈reify xx'∈id})
      (λ {x} x∈X → to ∈dom $ x , to ⟨,⟩∈reify (x∈X , x∈X , refl)) ,
    id-is-pfun
  
  open import ZF.Foundation.Axiom.Nonconstructive
  open import Relation.Nullary
  
  ∘-fun : g ∶ Y ⟶ Z → f ∶ X ⟶ Y → g ∘ f ∶ X ⟶ Z
  ∘-fun {g}{f = f} fung@(refl , g⇀) funf@(refl , f⇀) =
    antisym-⊆
      (λ x∈domgf → case from ∈dom x∈domgf of λ
        { (v , xv∈gf) → proj₁ $ from ⟨,⟩∈reify xv∈gf })
      (λ {x} x∈domf → case from ∈dom x∈domf of λ
        { (fx , xfx∈f) → case proj₂ $ to (funf [ x ]≡) xfx∈f of λ
        { refl → let fxgfx∈g = fung [ val∈ran funf x∈domf ∈dom] in
          to ∈dom $
          g [ fx ] ,
          to ⟨,⟩∈reify (x∈domf , to ∈ran (fx , fxgfx∈g) , fx , xfx∈f , fxgfx∈g)
        }}) ,
    ∘-pfun g⇀ f⇀
  
  ∘[]≡ : g ∶ Y ⟶ Z → f ∶ X ⟶ Y → ⋀ x ∈ X , g ∘ f [ x ] ≡ g [ f [ x ] ]
  ∘[]≡ {f = f} fung@(refl , g-fun , _) funf@(refl , f-fun , _) {x} x∈X =
    let fun∘ = ∘-fun fung funf
        fxgfx∈g = fung [ val∈ran funf x∈X ∈dom] in
    proj₂ $ to (fun∘ [ x ]≡) $ to ⟨,⟩∈reify $
    x∈X , to ∈ran (f [ x ] , fxgfx∈g) , f [ x ] , funf [ x∈X ∈dom] , fxgfx∈g

  ∅-fun : ∅ ∶ ∅ ⟶ Y
  ∅-fun = dom∅ , ∅-pfun

  unit-fun : ｛ ⟨ x ، y ⟩ ｝ ∶ ｛ x ｝ ⟶ ｛ y ｝
  unit-fun {x}{y} = dom｛⟨ x , y ⟩｝ , unit-pfun

  const-fun : X × ｛ y ｝ ∶ X ⟶ ｛ y ｝
  const-fun {X} {y} = dom-× (y , to ∈｛ y ｝ refl) , const-pfun

  ⊆-fun : ∀{X Y f g} → f ∶ X ⟶ Y → g ⊆ f → g ∶ dom g ⟶ Y
  ⊆-fun (refl , !f , f-rel) g⊆f =
    refl ,
    (λ xy∈g xy'∈g → !f (g⊆f xy∈g) (g⊆f xy'∈g)) ,
    λ z∈g → case from ∈× $ f-rel $ g⊆f z∈g of λ
      { (x , x∈domf , y , y∈Y , refl) → to ⟨,⟩∈× $
      to ∈dom (y , z∈g) , y∈Y}

  ⊆[]≡ : g ⊆ f → f ∶ X ⟶ Y → ⋀ x ∈ dom g , g [ x ] ≡ f [ x ]
  ⊆[]≡ {g}{f} g⊆f f-fun {x} x∈domg = case from ∈dom x∈domg of λ {(y , xy∈g) →
    begin g [ x ] ≡⟨ proj₂ $ to (g-fun [ x ]≡) xy∈g ⟩
          y ≡⟨ sym $ proj₂ $ to (f-fun [ x ]≡) $ g⊆f xy∈g ⟩
          f [ x ] ∎}
    where g-fun = ⊆-fun f-fun g⊆f

  ∪-fun : disjoint X X' → g ∶ X ⟶ Y → f ∶ X' ⟶ Y' → g ∪ f ∶ X ∪ X' ⟶ Y ∪ Y'
  ∪-fun {g = g}{f = f} disj (refl , g-pfun) (refl , f-pfun) =
    dom∪ g f , ∪-pfun disj g-pfun f-pfun

  fun-reify : (f : set → set)(X : set) → set
  fun-reify f X = ｛ ⟨ x ، f x ⟩ ∣ x ∈ X ｝

  fun-fun-reify : ∀{f} → fun-reify f X ∶ X ⟶ ran (fun-reify f X)
  fun-fun-reify {f = f} =
    antisym-⊆ ((λ { (y , xy∈r) → case from ∈r xy∈r of λ
                  { (x' , x'∈X , eq) → case proj₁ (from ⟨,⟩≡ eq) ∶ x' ≡ _ of λ
                  { refl → x'∈X}}})
              o from ∈dom)
              (λ {x} x∈X → to ∈dom (f x , to ∈r (x , x∈X , refl))) ,
    (λ xy∈r xy'∈r → case from ∈r xy∈r , from ∈r xy'∈r of λ
      { ((x , x∈X , eq) , x' , x'∈X , eq') →
        case from ⟨,⟩≡ eq , from ⟨,⟩≡ eq' of λ
      { ((refl , refl) , refl , refl) → refl}}) ,
    λ x∈r → case from ∈r x∈r of λ{ (x , x∈X , refl) →
      to ⟨,⟩∈× $ x∈X , to ∈ran (x , x∈r)}
    where ∈r = ∈fun-rep λ x → ⟨ x ، f x ⟩
  
  valid-fun-reify : ∀{f X} → ⋀ x ∈ X , fun-reify f X [ x ] ≡ f x
  valid-fun-reify {f}{X}{x} x∈X =
    proj₂ $ to (fun-fun-reify [ x ]≡) $
    to (∈fun-rep λ x → ⟨ x ، f x ⟩) $ x , x∈X , refl

  id[]≡ : ⋀ x ∈ X , id X [ x ] ≡ x
  id[]≡ {X}{x} x∈X = proj₂ $ to (id-fun [ x ]≡) $ to ⟨,⟩∈reify $
                     x∈X , x∈X , refl

  open import Function.Equivalence.Reasoning as ⇔ using (step-⇔)
  open import Logic.Properties
  
  ∈dom-fun-reify : ∀{f X x} → x ∈ dom (fun-reify f X) ⇔ x ∈ X
  ∈dom-fun-reify {f}{X}{x} = ⇔.begin
    x ∈ dom (fun-reify f X) ⇔⟨ ∈dom ⟩
    (∃ λ y → ⟨ x ، y ⟩ ∈ fun-reify f X) ⇔⟨ ∃-cong-⇔ $ ∈fun-rep (λ x → ⟨ x ، f x ⟩) ⟩
    (∃ λ y → ⋁ x' ∈ X , ⟨ x' ، f x' ⟩ ≡ ⟨ x ، y ⟩)
      ⇔⟨ ∃-cong-⇔ $ ∃-cong-⇔ $ ∧-cong-⇔ ⇔-refl ⟨,⟩≡ ⟩
    (∃ λ y → ⋁ x' ∈ X , (x' ≡ x ∧ f x' ≡ y))
      ⇔⟨ mk⇔ (λ {(_ , _ , x∈X , refl , _) → x∈X})
             (λ x∈X → f x , x , x∈X , refl , refl) ⟩
    x ∈ X ⇔.∎

  ∈ran-fun-reify : ∀{f X y} → y ∈ ran (fun-reify f X) ⇔ ⋁ x ∈ X , y ≡ f x
  ∈ran-fun-reify {f}{X}{y} = ⇔.begin
    y ∈ ran (fun-reify f X) ⇔⟨ ∈ran ⟩
    (∃ λ x → ⟨ x ، y ⟩ ∈ fun-reify f X) ⇔⟨ ∃-cong-⇔ $ ∈fun-rep (λ x → ⟨ x ، f x ⟩) ⟩
    (∃ λ x → ⋁ x' ∈ X , ⟨ x' ، f x' ⟩ ≡ ⟨ x ، y ⟩)
      ⇔⟨ ∃-cong-⇔ $ ∃-cong-⇔ $ ∧-cong-⇔ ⇔-refl ⟨,⟩≡ ⟩
    (∃ λ x → ⋁ x' ∈ X , (x' ≡ x ∧ f x' ≡ y))
      ⇔⟨ mk⇔ (λ {(_ , x , x∈X , _ , refl) → x , x∈X , refl})
             (λ {(x , x∈X , refl) → x , x , x∈X , refl , refl}) ⟩
    (⋁ x ∈ X , y ≡ f x) ⇔.∎

  fun-reify-cong-⊆ : ∀{f} → X ⊆ Y → fun-reify f X ⊆ fun-reify f Y
  fun-reify-cong-⊆ = rep-cong-⊆

retraction : (g f : set) → Set
retraction g f = g ∘ f ≡ id (dom f) 
  
section : (g f : set) → Set
section g f = f ∘ g ≡ id (dom g) 

bijection : f ∶ X ⟶ Y → Set
bijection {f}{X}{Y} _ = ∃ λ g → retraction g f ∧ section g f ∧ g ∶ Y ⟶ X

_≅_ : (X Y : set) → Set
X ≅ Y = ∃ λ f → (f ∶ X ⟶ Y) ∧ᵈ bijection

abstract
  biejction-id : bijection (id-fun {X})
  biejction-id {X} =
    id X ,
    (subst (λ Y → id X ∘ id X ≡ id Y) (sym dom-id) $ id-∘ id-rel) ,
    (subst (λ Y → id X ∘ id X ≡ id Y) (sym dom-id) $ id-∘ id-rel) ,
    id-fun
  
  biejction-∘ : {fung : g ∶ Y ⟶ Z}{funf : f ∶ X ⟶ Y} →
    bijection fung →
    bijection funf →
    bijection (∘-fun fung funf)
  biejction-∘ {g}{f = f}{fung = fung@(refl , _ , g-rel)}
              {funf@(refl , _ , f-rel)}
              (g⁻¹ , g⁻¹g≡id , gg⁻¹≡id , g⁻¹-fun@(refl , _ , g⁻¹-rel))
              (f⁻¹ , f⁻¹f≡id , ff⁻¹≡id , f⁻¹-fun@(domf⁻¹≡ , _)) =
    f⁻¹ ∘ g⁻¹ ,
    (begin (f⁻¹ ∘ g⁻¹) ∘ g ∘ f ≡⟨ ∘-assoc ⟩
           f⁻¹ ∘ g⁻¹ ∘ g ∘ f ≡⟨ cong (f⁻¹ ∘_) $ sym ∘-assoc ⟩
           f⁻¹ ∘ (g⁻¹ ∘ g) ∘ f ≡⟨ cong (λ x → f⁻¹ ∘ x ∘ f) g⁻¹g≡id ⟩
           f⁻¹ ∘ id _ ∘ f ≡⟨ cong (f⁻¹ ∘_) (id-∘ f-rel) ⟩
           f⁻¹ ∘ f ≡⟨ subst (λ A → f⁻¹ ∘ f ≡ id A)
                            (sym $ proj₁ $ ∘-fun fung funf) f⁻¹f≡id ⟩
           id _ ∎) ,
    (begin (g ∘ f) ∘ f⁻¹ ∘ g⁻¹ ≡⟨ ∘-assoc ⟩
           g ∘ f ∘ f⁻¹ ∘ g⁻¹ ≡⟨ cong (g ∘_) $ sym ∘-assoc ⟩
           g ∘ (f ∘ f⁻¹) ∘ g⁻¹ ≡⟨ cong (λ x → g ∘ x ∘ g⁻¹) ff⁻¹≡id ⟩
           g ∘ id _ ∘ g⁻¹ ≡⟨ cong (g ∘_) $
                             subst (λ A → id A ∘ g⁻¹ ≡ g⁻¹)
                                   (sym domf⁻¹≡) $
                             id-∘ g⁻¹-rel ⟩
           g ∘ g⁻¹ ≡⟨ subst (λ A → g ∘ g⁻¹ ≡ id A)
                            (sym $ proj₁ $ ∘-fun f⁻¹-fun g⁻¹-fun) gg⁻¹≡id ⟩
           id _ ∎) ,
    ∘-fun f⁻¹-fun g⁻¹-fun

  module WithBijection (f-fun : f ∶ X ⟶ Y)(bij : bijection f-fun) where
    f⁻¹ = proj₁ bij
    f⁻¹f≡id = proj₁ $ proj₂ bij
    ff⁻¹≡id = proj₁ $ proj₂ $ proj₂ bij
    f⁻¹-fun = proj₂ $ proj₂ $ proj₂ bij

    ff⁻¹[y]≡y : ⋀ y ∈ Y , f [ f⁻¹ [ y ] ] ≡ y
    ff⁻¹[y]≡y {y} y∈Y =
      begin f [ f⁻¹ [ y ] ] ≡⟨ sym $ ∘[]≡ f-fun f⁻¹-fun y∈Y ⟩
            f ∘ f⁻¹ [ y ] ≡⟨ cong (_[ y ]) ff⁻¹≡id ⟩
            id (dom f⁻¹) [ y ] ≡⟨ cong (λ X → id X [ y ]) $ proj₁ f⁻¹-fun ⟩
            id Y [ y ] ≡⟨ id[]≡ y∈Y ⟩
            y ∎

    f⁻¹f[x]≡x : ⋀ x ∈ X , f⁻¹ [ f [ x ] ] ≡ x
    f⁻¹f[x]≡x {x} x∈X =
      begin f⁻¹ [ f [ x ] ] ≡⟨ sym $ ∘[]≡ f⁻¹-fun f-fun x∈X ⟩
            f⁻¹ ∘ f [ x ] ≡⟨ cong (_[ x ]) f⁻¹f≡id ⟩
            id (dom f) [ x ] ≡⟨ cong (λ X → id X [ x ]) $ proj₁ f-fun ⟩
            id X [ x ] ≡⟨ id[]≡ x∈X ⟩
            x ∎

    surj : surjective f Y
    surj {y} y∈Y =
      f⁻¹ [ y ] ,
      from (f-fun [ f⁻¹ [ y ] ]≡) (val∈ran f⁻¹-fun y∈Y , ff⁻¹[y]≡y y∈Y)

    fun-inj : fun-injective f X
    fun-inj {x} x∈X {x'} x'∈X fx≡fx' =
      begin x ≡⟨ sym $ f⁻¹f[x]≡x x∈X ⟩
            f⁻¹ [ f [ x ] ] ≡⟨ cong (f⁻¹ [_]) fx≡fx' ⟩
            f⁻¹ [ f [ x' ] ] ≡⟨ f⁻¹f[x]≡x x'∈X ⟩
            x' ∎

    inj : injective f
    inj = from (fun-inj⇔inj f-fun) fun-inj

    ran≡ : ran f ≡ Y
    ran≡ = from (surj-ran $ fun→rel f-fun) surj
  
    bij⁻¹ : bijection f⁻¹-fun
    bij⁻¹ = f , ff⁻¹≡id , f⁻¹f≡id , f-fun

  -- functional images

  import Data.Product as Prod

  ∈f⃗[] : f ∶ X ⟶ Y → ∀{X'} → X' ⊆ X
    → ---------------------------------------------
    ∀{y} → y ∈ f ⃗[ X' ] ⇔ ⋁ x ∈ X' , f [ x ] ≡ y
  ∈f⃗[] {f} f-fun {X'} X'⊆X {y} = ⇔.begin
    y ∈ f ⃗[ X' ] ⇔⟨ ∈⃗[] ⟩
    (⋁ x ∈ X' , ⟨ x ، y ⟩ ∈ f)
      ⇔⟨ mk⇔ (Prod.map₂ $ Prod.map₂ $ proj₂ o to (f-fun [ _ ]≡))
             (λ { (x , x∈X' , f[x]≡y) →
               x , x∈X' , from (f-fun [ x ]≡) (X'⊆X x∈X' , f[x]≡y)}) ⟩
    (⋁ x ∈ X' , f [ x ] ≡ y) ⇔.∎ 

  ∈f⃖[] : (f-fun : f ∶ X ⟶ Y) → ∀{Y'}
    → ---------------------------------------------
    ⋀ x ∈ X , (x ∈ fun→rel f-fun ⃖[ Y' ] ⇔ f [ x ] ∈ Y')
  ∈f⃖[] {f}{X} f-fun@(_ , _ , f-rel) {Y'}{x} x∈X = ⇔.begin
    x ∈ f-rel ⃖[ Y' ] ⇔⟨ ∈⃖[] f-rel ⟩
    (x ∈ X ∧ ¬ ∃ λ y → ⟨ x ، y ⟩ ∈ f ∧ y ∉ Y')
      ⇔⟨ [B→A]→A∧B⇔B (λ _ → x∈X) ⟩
    (¬ ∃ λ y → ⟨ x ، y ⟩ ∈ f ∧ y ∉ Y')
      ⇔⟨ mk⇔ (λ {¬xy∈f → by-contradiction λ f[x]∉Y' → ¬xy∈f $
                f [ x ] , f-fun [ x∈X ∈dom] , f[x]∉Y'})
             (λ { f[x]∈Y' (y , xy∈f , y∉Y') →
                case proj₂ $ to (f-fun [ x ]≡) xy∈f of λ
                { refl → y∉Y' f[x]∈Y'}}) ⟩
    f [ x ] ∈ Y' ⇔.∎

  infix 25 _⃖
  _⃖ : R ∶ X ⇸ Y → set
  _⃖ {R}{X}{Y} R-rel = fun-reify (λ b → R-rel ⃖[ ｛ b ｝ ]) Y

  ⃖-fun : (R-rel : R ∶ X ⇸ Y) → R-rel ⃖ ∶ Y ⟶ 𝒫 X
  ⃖-fun {R}{X}{Y} R-rel = ∶⟶-cong-⊆ refl ranR⃖⊆𝒫X fun-fun-reify
    where
    ranR⃖⊆𝒫X : ran (R-rel ⃖) ⊆ 𝒫 X
    ranR⃖⊆𝒫X X'∈ranR⃖ = case from ∈ran-fun-reify  X'∈ranR⃖ of λ
      { (y , y∈Y , refl) → to ∈𝒫 $ proj₁ F.∘ from (∈⃖[] R-rel) }

  ⃖-fvalid : (f-fun : f ∶ X ⟶ Y) →
    ⋀ y ∈ Y , fun→rel f-fun ⃖ [ y ] ≡ ｛ x ∈ X ∣ f [ x ] ≡ y ｝
  ⃖-fvalid {f}{X}{Y} f-fun@(_ , _ , f-rel) {y} y∈Y = antisym-⊆
    (λ {x} x∈f⃖[y] →
      let f⃖[y]⊆X : f-rel ⃖ [ y ] ⊆ X
          f⃖[y]⊆X = from ∈𝒫 $ ran⊆ (fun→rel $ ⃖-fun f-rel) $
                    to ∈ran $ y , ⃖-fun f-rel [ y∈Y ∈dom]
          x∈X : x ∈ X
          x∈X = f⃖[y]⊆X x∈f⃖[y]
          x∈f⃖[[y]] = subst (x ∈_)(valid-fun-reify y∈Y) x∈f⃖[y]
      in to ∈sep $ x∈X , from ∈｛ y ｝(from (∈f⃖[] f-fun x∈X) x∈f⃖[[y]]))
    λ {x} x∈sep → case from ∈sep x∈sep of λ { (x∈X , refl) →
      let f[x]∈Y : f [ x ] ∈ Y
          f[x]∈Y = val∈ran f-fun x∈X
      in subst (x ∈_) (sym $ valid-fun-reify f[x]∈Y) $
         to (∈f⃖[] f-fun x∈X) $ to ∈｛ f [ x ] ｝ refl}
    where ∈sep = ∈｛ x ∈ X ∣ f [ x ] ≡ y ｝

  -- inverse

  f∘f⁻¹≡id : f ∶ X ⇀ Y → surjective f Y → f ∘ f ⁻¹ ≡ id Y
  f∘f⁻¹≡id {f}{X}{Y} f-pfun@(_ , f-rel) surj-f = antisym-⊆
    (from (⁻¹-⊆ reify-rel) $ begin⊆
      (f ∘ f ⁻¹) ⁻¹ ≡⟨ ∘⁻¹ ⟩
      f ⁻¹ ⁻¹ ∘ f ⁻¹ ⊆⟨ to R⁻¹∘R⊆id⇔inj $ ∶⇀→⁻¹-inj f-pfun ⟩
      id (dom (f ⁻¹)) ≡⟨ cong id dom⁻¹ ⟩
      id (ran f) ≡⟨ cong id (from (surj-ran f-rel) surj-f) ⟩
      id Y ≡⟨ sym id⁻¹ ⟩
      id Y ⁻¹ ∎)
    (to id⊆R∘R⁻¹⇔surj surj-f)

  f⁻¹∘f≡id : f ∶ X ⟶ Y → injective f → f ⁻¹ ∘ f ≡ id X
  f⁻¹∘f≡id {f} f-fun@(refl , _) inj-f = antisym-⊆
    (to R⁻¹∘R⊆id⇔inj inj-f)
    (from (⁻¹-⊆ id-rel) $ begin⊆
      id (dom f) ⁻¹ ≡⟨ id⁻¹ ⟩
      id (dom f) ≡⟨ cong id $ sym ran⁻¹ ⟩
      id (ran (f ⁻¹) ) ⊆⟨ to id⊆R∘R⁻¹⇔surj triv-surj ⟩
      f ⁻¹ ∘ f ⁻¹ ⁻¹ ≡⟨ sym ∘⁻¹ ⟩
      (f ⁻¹ ∘ f)⁻¹ ∎)

module Bijection (f-fun : f ∶ X ⟶ Y)
                 (inj-f : injective f)
                 (surj-f : surjective f Y) where
  private
    domf≡ = proj₁ f-fun
    f-pfun = fun→pfun f-fun
    f-rel = fun→rel f-fun

  ⁻¹-fun : f ⁻¹ ∶ Y ⟶ X
  ⁻¹-fun =
    ∶⇀-⁻¹surj→∶⟶ f⁻¹∶Y⇀X $ subst (λ g → surjective g Y) f≡f⁻¹⁻¹ surj-f
    where
      f≡f⁻¹⁻¹ : f ≡ f ⁻¹ ⁻¹
      f≡f⁻¹⁻¹ = sym $ ⁻¹-involutive f-rel
      f⁻¹-rel : f ⁻¹ ∶ Y ⇸ X
      f⁻¹-rel = ⁻¹-rel f-rel
      f⁻¹∶Y⇀X : f ⁻¹ ∶ Y ⇀ X
      f⁻¹∶Y⇀X = rel-⁻¹inj→∶⇀ f⁻¹-rel $
                subst injective f≡f⁻¹⁻¹ inj-f

  bij : bijection f-fun
  bij with refl ← domf≡ | refl ← proj₁ ⁻¹-fun =
    f ⁻¹ , f⁻¹∘f≡id f-fun inj-f , f∘f⁻¹≡id f-pfun surj-f , ⁻¹-fun

  f[f⁻¹[y]]≡y : ⋀ y ∈ Y , f [ f ⁻¹ [ y ] ] ≡ y
  f[f⁻¹[y]]≡y {y} y∈Y = begin
    f [ f ⁻¹ [ y ] ] ≡⟨ sym $ ∘[]≡ f-fun ⁻¹-fun y∈Y ⟩
    f ∘ f ⁻¹ [ y ] ≡⟨ cong (_[ y ]) $ f∘f⁻¹≡id f-pfun surj-f ⟩
    id Y [ y ] ≡⟨ id[]≡ y∈Y ⟩
    y ∎

  f⁻¹[f[x]]≡y : ⋀ x ∈ X , f ⁻¹ [ f [ x ] ] ≡ x
  f⁻¹[f[x]]≡y {x} x∈X = begin
    f ⁻¹ [ f [ x ] ] ≡⟨ sym $ ∘[]≡ ⁻¹-fun f-fun x∈X ⟩
    f ⁻¹ ∘ f [ x ] ≡⟨ cong (_[ x ]) $ f⁻¹∘f≡id f-fun inj-f ⟩
    id X [ x ] ≡⟨ id[]≡ x∈X ⟩
    x ∎

-- function domain restriction
restrict : (f X : set) → set
restrict f X = RelBase.restrict f X (ran f)

open import Function hiding (id; _∘_; _⟶_)

abstract
  restrict-fun : X ⊆ X' → f ∶ X' ⟶ Y' → restrict f X ∶ X ⟶ Y'
  restrict-fun {X}{X'}{f}{Y'} X⊆X' (refl , f-fun , f-rel) =
    (begin dom f' ≡⟨ dom-restrict≡ ⟩
           dom f ∩ X ≡⟨ A⊆B→B∩A≡A X⊆X' ⟩
           X ∎) ,
    (restrict⊆ -⟨ f-fun ⟩- restrict⊆) ,
    ∶⇸-cong-⊆ (begin⊆ dom f' ⊆⟨ dom-restrict⊆ ⟩
                       dom f ∩ X ⊆⟨ A∩B⊆B ⟩
                       X ∎)
               (begin⊆ ran f' ⊆⟨ ran-restrict⊆ ⟩
                       ran f ∩ ran f ≡⟨ ∩-idemp ⟩
                       ran f ⊆⟨ ran⊆ f-rel ⟩
                       Y' ∎)
               (RelBase.tighten $ ⊆-rel f-rel restrict⊆)
    where f' = restrict f X

  restrict-to_[_]≡ :
    f ∶ X ⟶ Y → ∀{X'}
    → ----------------------------------------
    ⋀ x ∈ X ∩ X' , restrict f X' [ x ] ≡ f [ x ]
  restrict-to f-fun@(refl , _) [ x∈X'∩X ]≡ =
    ⊆[]≡ restrict⊆ f-fun $ subst (_ ∈_) (sym dom-restrict≡) x∈X'∩X

  -- restrict-fix : X ⊆ X' → f ∶ X ⟶ Y → restrict f X' ≡ f
  -- restrict-fix {X}{X'}{f} X⊆X' f-fun@(_ , _ , f-rel) =
  --   antisym-⊆ restrict⊆ λ {e} e∈f →
  --   to ∈r $ e∈f , (case from ∈× $ f-rel e∈f of λ
  --     { (x , x∈X , y , y∈Y , refl) →
  --       subst (_∈ X') (sym π₁⟨ x ، y ⟩) $ X⊆X' x∈X})
  --   where ∈r = ∈｛ e ∈ f ∣ π₁ e ∈ X' ｝

  -- restrict-restrict : X ⊆ Y → restrict (restrict f Y) X ≡ restrict f X
  -- restrict-restrict {X}{Y}{f} X⊆Y = begin
  --   restrict (restrict f Y) X ≡⟨ sep-sep ⟩
  --   ｛ e ∈ f ∣ π₁ e ∈ Y ∧ π₁ e ∈ X ｝ ≡⟨ sym $ sep-⇔ $ [B→A]→A∧B⇔B X⊆Y ⟩
  --   restrict f X ∎

  restrict≡restrict :
    X ⊆ dom f → f ⊆ g → g ∶ X' ⟶ Y'
    → ----------------------------------------
    restrict f X ≡ restrict g X
  restrict≡restrict {X}{f}{g} X⊆f f⊆g g-fun@(refl , _) =
    fun-ext restrict-f-fun restrict-g-fun λ {x} x∈X →
    begin restrict f X [ x ] ≡⟨ restrict-to f-fun [ to ∈∩ $ X⊆f x∈X , x∈X ]≡ ⟩
          f [ x ] ≡⟨ ⊆[]≡ f⊆g g-fun $ X⊆f x∈X ⟩
          g [ x ] ≡⟨ sym $ restrict-to g-fun [
                             to ∈∩ $ dom-cong-⊆ f⊆g (X⊆f x∈X) , x∈X ]≡ ⟩
          restrict g X [ x ] ∎
    where f-fun = ⊆-fun g-fun f⊆g
          X⊆g = trans-⊆ X⊆f $ dom-cong-⊆ f⊆g
          restrict-f-fun = restrict-fun X⊆f f-fun
          restrict-g-fun = restrict-fun X⊆g g-fun

