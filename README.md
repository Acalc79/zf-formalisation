# Implementation of axiomatic ZF(C) set theory

Based on multiple sources, in large part follows Cambridge Mathematical Tripos Part II Logic and Set Theory course notes from https://qk206.user.srcf.net/

All numbers and names in the comments refer to those notes
