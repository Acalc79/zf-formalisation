{-# OPTIONS --exact-split #-}
module ZF.Foundation.Relation.Base where

open import ZF.Foundation.Axiom
open import ZF.Foundation.Pair

Rel : (X Y : set) → set
Rel X Y = 𝒫 (X × Y)

private variable R S T X Y X' Y' X″ Y″ u v : set

infix 5 _∶_⇸_

_∶_⇸_ : (R X Y : set) → Set
R ∶ X ⇸ Y = R ⊆ X × Y

abstract
  ∈Rel : R ∈ Rel X Y ⇔ R ∶ X ⇸ Y
  ∈Rel = ∈𝒫

  Rel-cong-⊆ : X ⊆ X' → Y ⊆ Y' → Rel X Y ⊆ Rel X' Y'
  Rel-cong-⊆ = 𝒫-cong-⊆ ∘₂ ×-cong-⊆

  ∶⇸-cong-⊆ : X ⊆ X' → Y ⊆ Y' → R ∶ X ⇸ Y → R ∶ X' ⇸ Y'
  ∶⇸-cong-⊆ X⊆X' Y⊆Y' R⊆X×Y = ×-cong-⊆ X⊆X' Y⊆Y' ∘ R⊆X×Y

  ∅-rel : ∅ ∶ X ⇸ Y
  ∅-rel = ∅⊆ _

  ×-rel : X × Y ∶ X ⇸ Y
  ×-rel = refl-⊆

  ∪Rel : R ∶ X ⇸ Y → S ∶ X' ⇸ Y' → R ∪ S ∶ X ∪ X' ⇸ Y ∪ Y'
  ∪Rel r-rel s-rel = A⊆X∧B⊆X→A∪B⊆X
    (trans-⊆ r-rel (×-cong-⊆ A⊆A∪B A⊆A∪B))
    (trans-⊆ s-rel (×-cong-⊆ B⊆A∪B B⊆A∪B))

  dom ran : (R : set) → set
  dom R = ｛ u ∈ ⋃ (⋃ R) ∣ ∃[ v ] ⟨ u ، v ⟩ ∈ R ｝

  ran R = ｛ v ∈ ⋃ (⋃ R) ∣ ∃[ u ] ⟨ u ، v ⟩ ∈ R ｝

  ∈dom : u ∈ dom R ⇔ ∃ λ v → ⟨ u ، v ⟩ ∈ R
  ∈dom {u}{R} = mk⇔
    (proj₂ ∘ from ∈dom')
    (λ {(v , uv∈R) → to ∈dom' $
      to ∈⋃ (
        ｛ u ｝ ,
        to ∈⋃ (⟨ u ، v ⟩ , uv∈R , to ∈⟨,⟩ (inj₁ refl)) ,
        to ∈｛ u ｝ refl) ,
      v , uv∈R
    })
    where ∈dom' =  ∈｛ u ∈ ⋃ (⋃ R) ∣ ∃[ v ] ⟨ u ، v ⟩ ∈ R ｝

  dom∅ : dom ∅ ≡ ∅
  dom∅ = antisym-⊆ (⊥-elim ∘ ∈∅ ∘ proj₂ ∘ from ∈dom)
                   (∅⊆ $ dom ∅)

  dom⋃ : dom (⋃ X) ≡ ⋃ ｛ dom Y ∣ Y ∈ X ｝
  dom⋃ {X} = antisym-⊆
    (λ x∈dom⋃X → case from ∈dom x∈dom⋃X of λ
      {(y , xy∈⋃X) → case from ∈⋃ xy∈⋃X of λ
      {(Y , Y∈X , xy∈Y) → to ∈⋃ $
      dom Y , to (∈fun-rep dom) (Y , Y∈X , refl) , to ∈dom (y , xy∈Y)}})
    λ x∈⋃dom → case from ∈⋃ x∈⋃dom of λ { (domY , domY∈rep , x∈domY) →
      case from (∈fun-rep dom) domY∈rep of λ { (Y , Y∈X , refl) →
      case from ∈dom x∈domY of λ { (y , xy∈Y) → 
      to ∈dom (y , to ∈⋃ (Y , Y∈X , xy∈Y))}}}

  open ≡-Reasoning

  dom｛⟨_,_⟩｝ : ∀ u v → dom ｛ ⟨ u ، v ⟩ ｝ ≡ ｛ u ｝
  dom｛⟨ u , v ⟩｝ = antisym-⊆
    (λ x∈dom → case from ∈dom x∈dom of λ { (v' , xv'∈uv) →
      case from ⟨,⟩≡ $ from ∈｛ _ ｝ xv'∈uv of λ {(refl , _) →
      to ∈｛ u ｝ refl}})
    (λ x∈u → case from ∈｛ u ｝ x∈u of λ {refl →
      to ∈dom (v , to ∈｛ _ ｝ refl)})

  dom∪ : ∀ R S → dom (R ∪ S) ≡ dom R ∪ dom S
  dom∪ R S = begin
    dom (R ∪ S) ≡⟨ dom⋃ ⟩
    ⋃ ｛ dom Y ∣ Y ∈ ｛ R ، S ｝ ｝ ≡⟨ cong ⋃ $ rep dom ｛ R ، S ｝≡ ⟩
    ⋃ ｛ dom R ، dom S ｝ ≡⟨⟩
    dom R ∪ dom S ∎

  dom-× : ∀{X Y} → inhabited Y → dom (X × Y) ≡ X
  dom-× (y , y∈Y) = antisym-⊆
    (λ x∈dom → proj₁ $ from ⟨,⟩∈× $ prop $ from ∈dom x∈dom)
    (λ x∈X → to ∈dom $ y , to ⟨,⟩∈× (x∈X , y∈Y))

  dom-step : ∀ R u v → dom (R ∪ ｛ ⟨ u ، v ⟩ ｝) ≡ dom R ∪ ｛ u ｝
  dom-step R u v = begin
    dom (R ∪ ｛ ⟨ u ، v ⟩ ｝) ≡⟨ dom∪ R ｛ ⟨ u ، v ⟩ ｝ ⟩
    dom R ∪ dom ｛ ⟨ u ، v ⟩ ｝ ≡⟨ cong (dom R ∪_) dom｛⟨ u , v ⟩｝ ⟩
    dom R ∪ ｛ u ｝ ∎

  dom⊆ : R ∶ X ⇸ Y → dom R ⊆ X
  dom⊆ R-rel u∈domR = case from ∈dom u∈domR of λ
    { (v , uv∈R) → proj₁ $ from ⟨,⟩∈× $ R-rel uv∈R }

  import Data.Product as Prod

  dom-cong-⊆ : X ⊆ Y → dom X ⊆ dom Y
  dom-cong-⊆ X⊆Y = sep-cong-⊆ (⋃-cong-⊆ $ ⋃-cong-⊆ X⊆Y)(Prod.map₂ X⊆Y)

  ∈ran : v ∈ ran R ⇔ ∃ λ u → ⟨ u ، v ⟩ ∈ R
  ∈ran {v}{R} = mk⇔
    (proj₂ ∘ from ∈ran')
    (λ {(u , uv∈R) → to ∈ran' $
      to ∈⋃ (
        ｛ u ، v ｝ ,
        to ∈⋃ (⟨ u ، v ⟩ , uv∈R , to ∈⟨,⟩ (inj₂ refl)) ,
        to ∈｛ u ، v ｝ (inj₂ refl)) ,
      u , uv∈R
    })
    where ∈ran' =  ∈｛ v ∈ ⋃ (⋃ R) ∣ ∃[ u ] ⟨ u ، v ⟩ ∈ R ｝

  ran⊆ : R ∶ X ⇸ Y → ran R ⊆ Y
  ran⊆ R-rel v∈ranR = case from ∈ran v∈ranR of λ
    { (u , uv∈R) → proj₂ $ from ⟨,⟩∈× $ R-rel uv∈R }

  ran-cong-⊆ : X ⊆ Y → ran X ⊆ ran Y
  ran-cong-⊆ X⊆Y = sep-cong-⊆ (⋃-cong-⊆ $ ⋃-cong-⊆ X⊆Y)(Prod.map₂ X⊆Y)
