{-# OPTIONS --exact-split #-}
module ZF.Relation.Order.Well.Base where

open import ZF.Relation.Order.Total as Total
open import ZF.Foundation hiding (_[_∈dom])

-- Definition (Well-ordering)
is-well-ordering : (R X : set) → Set
is-well-ordering R X = well-founded R X ∧ is-total-order R X

private
  variable X X' Y Y' R I S G RX RY x y z : set

module WellOrdered (well-ordered : is-well-ordering R X) where
  open import ZF.Relation.Order.Base
  open import ZF.Relation.Base hiding (_∘_)
  open TotalOrder (proj₂ well-ordered) public
  open <-Reasoning hiding (begin_; _∎)
  
  abstract
    wf : well-founded R X
    wf = proj₁ well-ordered
  
  module MinDef (Y⊆X : Y ⊆ X)(nonempty-Y : nonempty Y) where
    abstract
      min : set
      min = elem $ wf (to ∈𝒫 Y⊆X) nonempty-Y
  
      min∈ : min ∈ Y
      min∈ = proj₁ $ prop $ wf (to ∈𝒫 Y⊆X) nonempty-Y
  
      min-def : ⋀ y ∈ Y , y ≮ min
      min-def = proj₂ $ prop $ wf (to ∈𝒫 Y⊆X) nonempty-Y

      min≤ : ⋀ y ∈ Y , min ≤ y
      min≤ y∈Y = ≮→≥ (Y⊆X y∈Y) (Y⊆X min∈) $ min-def y∈Y

      ≤min→≡min : ⋀ y ∈ Y , (y ≤ min → y ≡ min)
      ≤min→≡min {y} y∈Y y≤min = ≤-antisym y≤min (min≤ y∈Y)

  min-irrelevant : ∀ Y
    {Y⊆X Y⊆X' : Y ⊆ X}
    {Y≢∅ Y≢∅' : nonempty Y}
    → ----------------------------------------
    MinDef.min Y⊆X Y≢∅ ≡ MinDef.min Y⊆X' Y≢∅' 
  min-irrelevant Y {Y⊆X}{Y⊆X'}{Y≢∅}{Y≢∅'} =
    case <-trich (Y⊆X M0.min∈)(Y⊆X' M1.min∈) of λ
    { (inj₁ min0<min1) → ⊥-elim $ M1.min-def M0.min∈ min0<min1
    ; (inj₂ (inj₁ min0≡min1)) → min0≡min1
    ; (inj₂ (inj₂ min1<min0)) → ⊥-elim $ M0.min-def M1.min∈ min1<min0 }
    where module M0 = MinDef Y⊆X Y≢∅
          module M1 = MinDef Y⊆X' Y≢∅'

  open import ZF.Foundation.Axiom.Nonconstructive

  module _ {Y Z}(Y⊆Z : Y ⊆ Z)(Y⊆X : Y ⊆ X)(Z⊆X : Z ⊆ X)
                (nonempty-Y : nonempty Y)(nonempty-Z : nonempty Z) where
    private
      module MZ = MinDef Z⊆X nonempty-Z
      module MY = MinDef Y⊆X nonempty-Y

    ⊆-min-≤ : MZ.min ≤ MY.min
    ⊆-min-≤ = case <-trich (Z⊆X MZ.min∈) (Y⊆X MY.min∈) of λ
      { (inj₁ minz<miny) → inj₁ minz<miny
      ; (inj₂ (inj₁ minz≡miny)) → inj₂ minz≡miny
      ; (inj₂ (inj₂ minz<miny)) → ⊥-elim $ MZ.min-def (Y⊆Z MY.min∈) minz<miny}

    ⊆-min-< : MZ.min ∉ Y → MZ.min < MY.min
    ⊆-min-< minz∉Y = case ⊆-min-≤ of λ
      { (inj₁ minz<miny) → minz<miny
      ; (inj₂ minz≡miny) → ⊥-elim $ minz∉Y $ subst (_∈ Y)(sym minz≡miny) MY.min∈} 

  open Initial X R

  abstract
    well-ordered-⊆ : Y ⊆ X → is-well-ordering (restrict R Y Y) Y
    well-ordered-⊆ Y⊆X =
      (λ {S} S∈𝒫Y nonempty-S → case wf (𝒫-cong-⊆ Y⊆X S∈𝒫Y) nonempty-S of λ
        { (x , x∈X , y≮x) → x , x∈X ,
          λ {y} y∈S y<'x → y≮x y∈S $ restrict⊆ y<'x}) ,
      total-order-⊆ (proj₂ well-ordered) Y⊆X

  open import Function.Reasoning

  -- Proposition 2.2 (Proof by induction)
  module _ (S⊆X : S ⊆ X)(S-prop : ⋀ x ∈ X , ((∀{y} → y < x → y ∈ S) → x ∈ S)) where
    private
      X⊆S : X ⊆ S

    abstract
      X⊆S {x} x∈X = by-contradiction λ x∉S →
        let open MinDef {Y = X - S}
                        (proj₁ ∘ from ∈-)
                        (λ empty-X-S → empty-X-S $ to ∈- $ x∈X , x∉S) in
        S-prop (proj₁ $ from ∈- min∈)
               (λ y<min → by-contradiction λ y∉S →
                          min-def (to ∈- $ x<y→x∈X y<min , y∉S) y<min)
          ∶ min ∈ S
        |> proj₂ (from ∈- min∈) ∶ ⊥
      
      inductive≡ : S ≡ X
      inductive≡ = antisym-⊆ S⊆X X⊆S

  abstract
    init : set → set
    init x = ｛ y ∈ X ∣ y < x ｝

    init⊆ : init x ⊆ X
    init⊆ {x} = sep⊆ (_< x) X

    ∈init : ∀{x y} → y ∈ init x ⇔ y < x
    ∈init {x}{y} = begin
      y ∈ init x ⇔⟨ ∈｛ y ∈ X ∣ y < x ｝ ⟩
      (y ∈ X ∧ y < x) ⇔⟨ [B→A]→A∧B⇔B x<y→x∈X ⟩
      y < x ∎
      where open import Function.Equivalence.Reasoning
            open import Logic.Properties

    init-is-initial : init x is-initial
    init-is-initial {x} = sep⊆ (_< x) X ,
      λ {y} y∈init {z} z<y → to ∈init $ <-trans z<y $ from ∈init y∈init

    init⊆initial : I is-initial → x ∈ I → init x ⊆ I
    init⊆initial (_ , y<x→y∈I) x∈I {y} y∈init-x =
      y<x→y∈I x∈I $ from ∈init y∈init-x

    <-init⊆ : x < y → init x ⊆ init y
    <-init⊆ = init⊆initial init-is-initial ∘ to ∈init

    init-step : ⋀ x ∈ X , (init x ∪ ｛ x ｝) is-initial
    init-step {x} x∈X = A⊆X∧B⊆X→A∪B⊆X (sep⊆ (_< x) X) (to unit-⊆ x∈X) ,
      λ {y} y∈step {z} z<y → to ∈∪ $ inj₁ $ to ∈init $ case from ∈∪ y∈step of λ
      { (inj₁ y∈init) → <-trans z<y (from ∈init y∈init) ∶ z < x
      ; (inj₂ y∈x) → subst (z <_) (from ∈｛ x ｝ y∈x) z<y ∶ z < x}

    initial-is-init : I is-initial → I ≢ X → ∃ λ x → I ≡ init x
    initial-is-init {I} init-I@(I⊆X , x<y→x∈I) I≢X = min , antisym-⊆
      (λ x∈I → to ∈init $ case <-trich (I⊆X x∈I)(A-B⊆A min∈) of λ
        { (inj₁ x<min) → x<min
        ; (inj₂ (inj₁ refl)) → ⊥-elim $ proj₂ (from ∈- min∈) x∈I
        ; (inj₂ (inj₂ min<x)) → ⊥-elim $ proj₂ (from ∈- min∈) $ x<y→x∈I x∈I min<x
        })
      ((λ x<min → by-contradiction λ x∉I →
                  min-def (to ∈- $ x<y→x∈X x<min , x∉I) x<min)
        ∘ from ∈init)
      where S' = X - I
            open MinDef {Y = S'} A-B⊆A (λ ∀x∉S → I≢X $ antisym-⊆ I⊆X $
              A-B≡∅→A⊆B $ antisym-⊆ (⊥-elim ∘ ∀x∉S) (∅⊆ S'))
