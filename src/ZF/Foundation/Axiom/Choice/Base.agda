{-# OPTIONS --exact-split #-}
module ZF.Foundation.Axiom.Choice.Base where

open import ZF.Foundation.Axiom.Base

postulate
  choice-exists :
    ∀ F (p : ⋀ x ∈ F , nonempty x ∧ (⋀ y ∈ F , (x ≢ y → disjoint x y)))
    → --------------------------------------------------------------------
    ∃[ S ] (⋀ x ∈ F , ∃! λ z → z ∈ S ∧ z ∈ x)
